
(function(l, r) { if (!l || l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (self.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.getElementsByTagName('head')[0].appendChild(r) })(self.document);
var app = (function () {
    'use strict';

    function noop() { }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    let src_url_equal_anchor;
    function src_url_equal(element_src, url) {
        if (!src_url_equal_anchor) {
            src_url_equal_anchor = document.createElement('a');
        }
        src_url_equal_anchor.href = url;
        return element_src === src_url_equal_anchor.href;
    }
    function is_empty(obj) {
        return Object.keys(obj).length === 0;
    }
    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function svg_element(name) {
        return document.createElementNS('http://www.w3.org/2000/svg', name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_style(node, key, value, important) {
        node.style.setProperty(key, value, important ? 'important' : '');
    }
    // unfortunately this can't be a constant as that wouldn't be tree-shakeable
    // so we cache the result instead
    let crossorigin;
    function is_crossorigin() {
        if (crossorigin === undefined) {
            crossorigin = false;
            try {
                if (typeof window !== 'undefined' && window.parent) {
                    void window.parent.document;
                }
            }
            catch (error) {
                crossorigin = true;
            }
        }
        return crossorigin;
    }
    function add_resize_listener(node, fn) {
        const computed_style = getComputedStyle(node);
        if (computed_style.position === 'static') {
            node.style.position = 'relative';
        }
        const iframe = element('iframe');
        iframe.setAttribute('style', 'display: block; position: absolute; top: 0; left: 0; width: 100%; height: 100%; ' +
            'overflow: hidden; border: 0; opacity: 0; pointer-events: none; z-index: -1;');
        iframe.setAttribute('aria-hidden', 'true');
        iframe.tabIndex = -1;
        const crossorigin = is_crossorigin();
        let unsubscribe;
        if (crossorigin) {
            iframe.src = "data:text/html,<script>onresize=function(){parent.postMessage(0,'*')}</script>";
            unsubscribe = listen(window, 'message', (event) => {
                if (event.source === iframe.contentWindow)
                    fn();
            });
        }
        else {
            iframe.src = 'about:blank';
            iframe.onload = () => {
                unsubscribe = listen(iframe.contentWindow, 'resize', fn);
            };
        }
        append(node, iframe);
        return () => {
            if (crossorigin) {
                unsubscribe();
            }
            else if (unsubscribe && iframe.contentWindow) {
                unsubscribe();
            }
            detach(iframe);
        };
    }
    function toggle_class(element, name, toggle) {
        element.classList[toggle ? 'add' : 'remove'](name);
    }
    function custom_event(type, detail, bubbles = false) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, bubbles, false, detail);
        return e;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error('Function called outside component initialization');
        return current_component;
    }
    function onMount(fn) {
        get_current_component().$$.on_mount.push(fn);
    }
    function createEventDispatcher() {
        const component = get_current_component();
        return (type, detail) => {
            const callbacks = component.$$.callbacks[type];
            if (callbacks) {
                // TODO are there situations where events could be dispatched
                // in a server (non-DOM) environment?
                const event = custom_event(type, detail);
                callbacks.slice().forEach(fn => {
                    fn.call(component, event);
                });
            }
        };
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    let flushing = false;
    const seen_callbacks = new Set();
    function flush() {
        if (flushing)
            return;
        flushing = true;
        do {
            // first, call beforeUpdate functions
            // and update components
            for (let i = 0; i < dirty_components.length; i += 1) {
                const component = dirty_components[i];
                set_current_component(component);
                update(component.$$);
            }
            set_current_component(null);
            dirty_components.length = 0;
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        flushing = false;
        seen_callbacks.clear();
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    let outros;
    function group_outros() {
        outros = {
            r: 0,
            c: [],
            p: outros // parent group
        };
    }
    function check_outros() {
        if (!outros.r) {
            run_all(outros.c);
        }
        outros = outros.p;
    }
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
    }

    const globals = (typeof window !== 'undefined'
        ? window
        : typeof globalThis !== 'undefined'
            ? globalThis
            : global);
    function create_component(block) {
        block && block.c();
    }
    function mount_component(component, target, anchor, customElement) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        if (!customElement) {
            // onMount happens before the initial afterUpdate
            add_render_callback(() => {
                const new_on_destroy = on_mount.map(run).filter(is_function);
                if (on_destroy) {
                    on_destroy.push(...new_on_destroy);
                }
                else {
                    // Edge case - component was destroyed immediately,
                    // most likely as a result of a binding initialising
                    run_all(new_on_destroy);
                }
                component.$$.on_mount = [];
            });
        }
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, append_styles, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            on_disconnect: [],
            before_update: [],
            after_update: [],
            context: new Map(parent_component ? parent_component.$$.context : options.context || []),
            // everything else
            callbacks: blank_object(),
            dirty,
            skip_bound: false,
            root: options.target || parent_component.$$.root
        };
        append_styles && append_styles($$.root);
        let ready = false;
        $$.ctx = instance
            ? instance(component, options.props || {}, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if (!$$.skip_bound && $$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                const nodes = children(options.target);
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(nodes);
                nodes.forEach(detach);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor, options.customElement);
            flush();
        }
        set_current_component(parent_component);
    }
    /**
     * Base class for Svelte components. Used when dev=false.
     */
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set($$props) {
            if (this.$$set && !is_empty($$props)) {
                this.$$.skip_bound = true;
                this.$$set($$props);
                this.$$.skip_bound = false;
            }
        }
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, Object.assign({ version: '3.42.4' }, detail), true));
    }
    function append_dev(target, node) {
        dispatch_dev('SvelteDOMInsert', { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev('SvelteDOMInsert', { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev('SvelteDOMRemove', { node });
        detach(node);
    }
    function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
        const modifiers = options === true ? ['capture'] : options ? Array.from(Object.keys(options)) : [];
        if (has_prevent_default)
            modifiers.push('preventDefault');
        if (has_stop_propagation)
            modifiers.push('stopPropagation');
        dispatch_dev('SvelteDOMAddEventListener', { node, event, handler, modifiers });
        const dispose = listen(node, event, handler, options);
        return () => {
            dispatch_dev('SvelteDOMRemoveEventListener', { node, event, handler, modifiers });
            dispose();
        };
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev('SvelteDOMRemoveAttribute', { node, attribute });
        else
            dispatch_dev('SvelteDOMSetAttribute', { node, attribute, value });
    }
    function prop_dev(node, property, value) {
        node[property] = value;
        dispatch_dev('SvelteDOMSetProperty', { node, property, value });
    }
    function set_data_dev(text, data) {
        data = '' + data;
        if (text.wholeText === data)
            return;
        dispatch_dev('SvelteDOMSetData', { node: text, data });
        text.data = data;
    }
    function validate_each_argument(arg) {
        if (typeof arg !== 'string' && !(arg && typeof arg === 'object' && 'length' in arg)) {
            let msg = '{#each} only iterates over array-like objects.';
            if (typeof Symbol === 'function' && arg && Symbol.iterator in arg) {
                msg += ' You can use a spread to convert this iterable into an array.';
            }
            throw new Error(msg);
        }
    }
    function validate_slots(name, slot, keys) {
        for (const slot_key of Object.keys(slot)) {
            if (!~keys.indexOf(slot_key)) {
                console.warn(`<${name}> received an unexpected slot "${slot_key}".`);
            }
        }
    }
    /**
     * Base class for Svelte components with some minor dev-enhancements. Used when dev=true.
     */
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error("'target' is a required option");
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn('Component was already destroyed'); // eslint-disable-line no-console
            };
        }
        $capture_state() { }
        $inject_state() { }
    }

    var sequence = [
      {
        start: 33,
        end: 34,
        type: 'Foodporn',
      },
      // {
      //   start: 4,
      //   end: 4.1,
      //   type: 'SpamHell',
      // },
      // {
      //   start: 1,
      //   end: 1.1,
      //   type: 'DamnbookFriends',
      // },
      // {
      //   start: 5,
      //   end: 5.1,
      //   type: 'InstagramScroll',
      // },
      // {
      //   start: 6,
      //   end: 6.1,
      //   type: 'InstagramScroll',
      // },
      {
        start: 1,
        end: 1.1,
        type: 'InstagramMask',
      },
      // {
      //   start: 1,
      //   end: 1.1,
      //   type: 'Quiz',
      // },
    ];

    /* src/components/Zone.svelte generated by Svelte v3.42.4 */
    const file$n = "src/components/Zone.svelte";

    function create_fragment$n(ctx) {
    	let a;
    	let div;
    	let a_href_value;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			a = element("a");
    			div = element("div");
    			attr_dev(div, "class", "zone shine svelte-11l9k4l");
    			attr_dev(div, "style", /*style*/ ctx[1]);
    			toggle_class(div, "whiteShadow", /*item*/ ctx[0].color === 'white');
    			add_location(div, file$n, 32, 2, 893);
    			attr_dev(a, "target", "_blank");
    			attr_dev(a, "rel", "nofollow");
    			attr_dev(a, "href", a_href_value = /*item*/ ctx[0].href);
    			add_location(a, file$n, 31, 0, 820);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, a, anchor);
    			append_dev(a, div);

    			if (!mounted) {
    				dispose = listen_dev(a, "click", /*onClick*/ ctx[2], false, false, false);
    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*style*/ 2) {
    				attr_dev(div, "style", /*style*/ ctx[1]);
    			}

    			if (dirty & /*item*/ 1) {
    				toggle_class(div, "whiteShadow", /*item*/ ctx[0].color === 'white');
    			}

    			if (dirty & /*item*/ 1 && a_href_value !== (a_href_value = /*item*/ ctx[0].href)) {
    				attr_dev(a, "href", a_href_value);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(a);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$n.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$n($$self, $$props, $$invalidate) {
    	let borderRadius;
    	let style;
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Zone', slots, []);
    	let { item } = $$props;
    	let { myPlayer } = $$props;
    	let { playerWidth } = $$props;
    	let timeoutId;

    	onMount(() => {
    		if (item.pause) {
    			myPlayer.pause();
    			document.querySelector('.vjs-control-bar').style.visibility = 'hidden';

    			timeoutId = setTimeout(
    				() => {
    					document.querySelector('.vjs-control-bar').style.visibility = 'visible';
    					myPlayer.play();
    				},
    				3000
    			);
    		}
    	});

    	const onClick = e => {
    		clearTimeout(timeoutId);
    		document.querySelector('.vjs-control-bar').style.visibility = 'visible';
    		myPlayer.pause();
    	};

    	const writable_props = ['item', 'myPlayer', 'playerWidth'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Zone> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('item' in $$props) $$invalidate(0, item = $$props.item);
    		if ('myPlayer' in $$props) $$invalidate(3, myPlayer = $$props.myPlayer);
    		if ('playerWidth' in $$props) $$invalidate(4, playerWidth = $$props.playerWidth);
    	};

    	$$self.$capture_state = () => ({
    		onMount,
    		item,
    		myPlayer,
    		playerWidth,
    		timeoutId,
    		onClick,
    		borderRadius,
    		style
    	});

    	$$self.$inject_state = $$props => {
    		if ('item' in $$props) $$invalidate(0, item = $$props.item);
    		if ('myPlayer' in $$props) $$invalidate(3, myPlayer = $$props.myPlayer);
    		if ('playerWidth' in $$props) $$invalidate(4, playerWidth = $$props.playerWidth);
    		if ('timeoutId' in $$props) timeoutId = $$props.timeoutId;
    		if ('borderRadius' in $$props) $$invalidate(5, borderRadius = $$props.borderRadius);
    		if ('style' in $$props) $$invalidate(1, style = $$props.style);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*item, playerWidth*/ 17) {
    			$$invalidate(5, borderRadius = item.withoutBorder ? 0 : playerWidth * 0.05);
    		}

    		if ($$self.$$.dirty & /*item, borderRadius*/ 33) {
    			$$invalidate(1, style = `width:${item.width}%; left:${item.left}%; top: ${item.top}%; padding-top: ${item.paddingTop}%; border-radius:${borderRadius}px;`);
    		}
    	};

    	return [item, style, onClick, myPlayer, playerWidth, borderRadius];
    }

    class Zone extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$n, create_fragment$n, safe_not_equal, { item: 0, myPlayer: 3, playerWidth: 4 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Zone",
    			options,
    			id: create_fragment$n.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*item*/ ctx[0] === undefined && !('item' in props)) {
    			console.warn("<Zone> was created without expected prop 'item'");
    		}

    		if (/*myPlayer*/ ctx[3] === undefined && !('myPlayer' in props)) {
    			console.warn("<Zone> was created without expected prop 'myPlayer'");
    		}

    		if (/*playerWidth*/ ctx[4] === undefined && !('playerWidth' in props)) {
    			console.warn("<Zone> was created without expected prop 'playerWidth'");
    		}
    	}

    	get item() {
    		throw new Error("<Zone>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set item(value) {
    		throw new Error("<Zone>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get myPlayer() {
    		throw new Error("<Zone>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set myPlayer(value) {
    		throw new Error("<Zone>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get playerWidth() {
    		throw new Error("<Zone>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set playerWidth(value) {
    		throw new Error("<Zone>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/Timer.svelte generated by Svelte v3.42.4 */
    const file$m = "src/components/Timer.svelte";

    function create_fragment$m(ctx) {
    	let svg;
    	let path0;
    	let path1;
    	let path1_stroke_dasharray_value;
    	let text_1;
    	let t;

    	const block = {
    		c: function create() {
    			svg = svg_element("svg");
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			text_1 = svg_element("text");
    			t = text(/*seconds*/ ctx[0]);
    			attr_dev(path0, "class", "timerBackground svelte-1u79q7a");
    			attr_dev(path0, "d", "M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0\n      -31.831");
    			attr_dev(path0, "fill", "none");
    			attr_dev(path0, "stroke-width", "1");
    			attr_dev(path0, "stroke-dasharray", "100, 100");
    			add_location(path0, file$m, 30, 4, 775);
    			attr_dev(path1, "class", "timer svelte-1u79q7a");
    			attr_dev(path1, "d", "M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0\n      -31.831");
    			attr_dev(path1, "fill", "none");
    			attr_dev(path1, "stroke", "#444");
    			attr_dev(path1, "stroke-width", "1");
    			attr_dev(path1, "stroke-dasharray", path1_stroke_dasharray_value = `${/*percent*/ ctx[1]}, 100`);
    			add_location(path1, file$m, 37, 4, 987);
    			attr_dev(text_1, "x", "18");
    			attr_dev(text_1, "y", "23");
    			attr_dev(text_1, "class", "text svelte-1u79q7a");
    			attr_dev(text_1, "text-anchor", "middle");
    			add_location(text_1, file$m, 45, 4, 1218);
    			attr_dev(svg, "viewBox", "0 0 36 36");
    			attr_dev(svg, "class", "timerSvg svelte-1u79q7a");
    			add_location(svg, file$m, 29, 2, 728);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, svg, anchor);
    			append_dev(svg, path0);
    			append_dev(svg, path1);
    			append_dev(svg, text_1);
    			append_dev(text_1, t);
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*percent*/ 2 && path1_stroke_dasharray_value !== (path1_stroke_dasharray_value = `${/*percent*/ ctx[1]}, 100`)) {
    				attr_dev(path1, "stroke-dasharray", path1_stroke_dasharray_value);
    			}

    			if (dirty & /*seconds*/ 1) set_data_dev(t, /*seconds*/ ctx[0]);
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(svg);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$m.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    const TURN_TIME = 5000;

    function instance$m($$self, $$props, $$invalidate) {
    	let percent;
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Timer', slots, []);
    	const dispatch = createEventDispatcher();
    	let time = 5000;
    	let seconds = 5;

    	onMount(() => {
    		let timerId = null;

    		const tick = () => {
    			$$invalidate(2, time = time - 10);

    			if (time === 0) {
    				dispatch("timerend", null);
    				return;
    			}

    			timerId = setTimeout(tick, 10);
    		};

    		tick();
    		return () => clearTimeout(timerId);
    	});

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Timer> was created with unknown prop '${key}'`);
    	});

    	$$self.$capture_state = () => ({
    		onMount,
    		createEventDispatcher,
    		dispatch,
    		TURN_TIME,
    		time,
    		seconds,
    		percent
    	});

    	$$self.$inject_state = $$props => {
    		if ('time' in $$props) $$invalidate(2, time = $$props.time);
    		if ('seconds' in $$props) $$invalidate(0, seconds = $$props.seconds);
    		if ('percent' in $$props) $$invalidate(1, percent = $$props.percent);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*time*/ 4) {
    			$$invalidate(1, percent = time / TURN_TIME * 100);
    		}

    		if ($$self.$$.dirty & /*time*/ 4) {
    			{
    				const secondsVal = Math.ceil(time / 1000);
    				$$invalidate(0, seconds = secondsVal); //secondsVal > 9 ? secondsVal : "0" + secondsVal;
    			}
    		}
    	};

    	return [seconds, percent, time];
    }

    class Timer extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$m, create_fragment$m, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Timer",
    			options,
    			id: create_fragment$m.name
    		});
    	}
    }

    /* src/components/Frame.svelte generated by Svelte v3.42.4 */
    const file$l = "src/components/Frame.svelte";

    // (16:0) {#if visible}
    function create_if_block$b(ctx) {
    	let div1;
    	let iframe;
    	let iframe_src_value;
    	let t;
    	let div0;
    	let timer;
    	let current;
    	timer = new Timer({ $$inline: true });
    	timer.$on("timerend", /*continuePlay*/ ctx[2]);

    	const block = {
    		c: function create() {
    			div1 = element("div");
    			iframe = element("iframe");
    			t = space();
    			div0 = element("div");
    			create_component(timer.$$.fragment);
    			if (!src_url_equal(iframe.src, iframe_src_value = /*item*/ ctx[0].link)) attr_dev(iframe, "src", iframe_src_value);
    			attr_dev(iframe, "frameborder", "0");
    			attr_dev(iframe, "allow", "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture");
    			iframe.allowFullscreen = "";
    			attr_dev(iframe, "title", "externalLink");
    			attr_dev(iframe, "class", "svelte-1ksimpo");
    			add_location(iframe, file$l, 17, 2, 342);
    			attr_dev(div0, "class", "timer svelte-1ksimpo");
    			add_location(div0, file$l, 24, 2, 546);
    			attr_dev(div1, "class", "container svelte-1ksimpo");
    			add_location(div1, file$l, 16, 0, 316);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div1, anchor);
    			append_dev(div1, iframe);
    			append_dev(div1, t);
    			append_dev(div1, div0);
    			mount_component(timer, div0, null);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			if (!current || dirty & /*item*/ 1 && !src_url_equal(iframe.src, iframe_src_value = /*item*/ ctx[0].link)) {
    				attr_dev(iframe, "src", iframe_src_value);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(timer.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(timer.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div1);
    			destroy_component(timer);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$b.name,
    		type: "if",
    		source: "(16:0) {#if visible}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$l(ctx) {
    	let if_block_anchor;
    	let current;
    	let if_block = /*visible*/ ctx[1] && create_if_block$b(ctx);

    	const block = {
    		c: function create() {
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (/*visible*/ ctx[1]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);

    					if (dirty & /*visible*/ 2) {
    						transition_in(if_block, 1);
    					}
    				} else {
    					if_block = create_if_block$b(ctx);
    					if_block.c();
    					transition_in(if_block, 1);
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				group_outros();

    				transition_out(if_block, 1, 1, () => {
    					if_block = null;
    				});

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$l.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$l($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Frame', slots, []);
    	let { item } = $$props;
    	let { myPlayer } = $$props;
    	let visible = true;

    	onMount(() => {
    		myPlayer.pause();
    	});

    	const continuePlay = () => {
    		$$invalidate(1, visible = false);
    		myPlayer.play();
    	};

    	const writable_props = ['item', 'myPlayer'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Frame> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('item' in $$props) $$invalidate(0, item = $$props.item);
    		if ('myPlayer' in $$props) $$invalidate(3, myPlayer = $$props.myPlayer);
    	};

    	$$self.$capture_state = () => ({
    		onMount,
    		Timer,
    		item,
    		myPlayer,
    		visible,
    		continuePlay
    	});

    	$$self.$inject_state = $$props => {
    		if ('item' in $$props) $$invalidate(0, item = $$props.item);
    		if ('myPlayer' in $$props) $$invalidate(3, myPlayer = $$props.myPlayer);
    		if ('visible' in $$props) $$invalidate(1, visible = $$props.visible);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [item, visible, continuePlay, myPlayer];
    }

    class Frame extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$l, create_fragment$l, safe_not_equal, { item: 0, myPlayer: 3 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Frame",
    			options,
    			id: create_fragment$l.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*item*/ ctx[0] === undefined && !('item' in props)) {
    			console.warn("<Frame> was created without expected prop 'item'");
    		}

    		if (/*myPlayer*/ ctx[3] === undefined && !('myPlayer' in props)) {
    			console.warn("<Frame> was created without expected prop 'myPlayer'");
    		}
    	}

    	get item() {
    		throw new Error("<Frame>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set item(value) {
    		throw new Error("<Frame>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get myPlayer() {
    		throw new Error("<Frame>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set myPlayer(value) {
    		throw new Error("<Frame>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/SpamHell/index.svelte generated by Svelte v3.42.4 */
    const file$k = "src/components/SpamHell/index.svelte";

    function get_each_context$6(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[8] = list[i];
    	return child_ctx;
    }

    // (71:8) {#if banners[name]?.isOpen}
    function create_if_block$a(ctx) {
    	let div;
    	let img;
    	let img_src_value;
    	let t;
    	let mounted;
    	let dispose;

    	function click_handler() {
    		return /*click_handler*/ ctx[5](/*name*/ ctx[8]);
    	}

    	const block = {
    		c: function create() {
    			div = element("div");
    			img = element("img");
    			t = space();
    			if (!src_url_equal(img.src, img_src_value = "./images/spamhell/banners/" + /*name*/ ctx[8] + ".png")) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "alt", "Spamhell " + /*name*/ ctx[8]);
    			add_location(img, file$k, 76, 16, 1903);
    			attr_dev(div, "class", "banner svelte-1w5gw5b");
    			set_style(div, "left", /*banners*/ ctx[0][/*name*/ ctx[8]].left + "px");
    			set_style(div, "top", /*banners*/ ctx[0][/*name*/ ctx[8]].top + "px");
    			add_location(div, file$k, 71, 12, 1705);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, img);
    			append_dev(div, t);

    			if (!mounted) {
    				dispose = listen_dev(div, "click", click_handler, false, false, false);
    				mounted = true;
    			}
    		},
    		p: function update(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (dirty & /*banners*/ 1) {
    				set_style(div, "left", /*banners*/ ctx[0][/*name*/ ctx[8]].left + "px");
    			}

    			if (dirty & /*banners*/ 1) {
    				set_style(div, "top", /*banners*/ ctx[0][/*name*/ ctx[8]].top + "px");
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$a.name,
    		type: "if",
    		source: "(71:8) {#if banners[name]?.isOpen}",
    		ctx
    	});

    	return block;
    }

    // (70:4) {#each bannerNames as name}
    function create_each_block$6(ctx) {
    	let if_block_anchor;
    	let if_block = /*banners*/ ctx[0][/*name*/ ctx[8]]?.isOpen && create_if_block$a(ctx);

    	const block = {
    		c: function create() {
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},
    		m: function mount(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    		},
    		p: function update(ctx, dirty) {
    			if (/*banners*/ ctx[0][/*name*/ ctx[8]]?.isOpen) {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block$a(ctx);
    					if_block.c();
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		d: function destroy(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$6.name,
    		type: "each",
    		source: "(70:4) {#each bannerNames as name}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$k(ctx) {
    	let div;
    	let each_value = /*bannerNames*/ ctx[1];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$6(get_each_context$6(ctx, each_value, i));
    	}

    	const block = {
    		c: function create() {
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(div, "class", "spamhell svelte-1w5gw5b");
    			add_location(div, file$k, 68, 0, 1602);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*banners, bannerNames, closeBanner*/ 7) {
    				each_value = /*bannerNames*/ ctx[1];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$6(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$6(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$k.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$k($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('SpamHell', slots, []);
    	let { myPlayer } = $$props;
    	let { playerWidth } = $$props;

    	const bannerNames = [
    		'Group 255',
    		'Group 259',
    		'Group 263',
    		'Group 264',
    		'Group 265',
    		'Group 267',
    		'Group 277',
    		'Group 278',
    		'Group 279',
    		'Group 284',
    		'Group 307',
    		'Group 308',
    		'Group 310',
    		'Group 311',
    		'Group 313',
    		'Group 315',
    		'Group 318',
    		'Group 320',
    		'Group 321',
    		'Group 322',
    		'Group 323',
    		'Group 324',
    		'Group 325',
    		'Group 326',
    		'Group 423'
    	];

    	let banners = {};

    	const setBannersOpen = (i = 0) => {
    		const name = bannerNames[i];
    		$$invalidate(0, banners[name].isOpen = true, banners);
    		if (i < bannerNames.length - 1) setTimeout(() => setBannersOpen(i + 1), Math.random() * 50);
    	};

    	onMount(() => {
    		myPlayer.pause();
    		myPlayer.controls(false);

    		bannerNames.forEach(name => {
    			$$invalidate(
    				0,
    				banners[name] = {
    					isOpen: null,
    					left: Math.random() * playerWidth * 0.9 - playerWidth * 0.1,
    					top: Math.random() * playerWidth * 360 / 640 * 0.9 - playerWidth * 360 / 640 * 0.1
    				},
    				banners
    			);
    		});

    		setBannersOpen();
    	});

    	const continuePlay = () => {
    		myPlayer.controls(true);
    		myPlayer.play();
    	};

    	const closeBanner = name => $$invalidate(0, banners[name].isOpen = false, banners);
    	const writable_props = ['myPlayer', 'playerWidth'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<SpamHell> was created with unknown prop '${key}'`);
    	});

    	const click_handler = name => closeBanner(name);

    	$$self.$$set = $$props => {
    		if ('myPlayer' in $$props) $$invalidate(3, myPlayer = $$props.myPlayer);
    		if ('playerWidth' in $$props) $$invalidate(4, playerWidth = $$props.playerWidth);
    	};

    	$$self.$capture_state = () => ({
    		onMount,
    		myPlayer,
    		playerWidth,
    		bannerNames,
    		banners,
    		setBannersOpen,
    		continuePlay,
    		closeBanner
    	});

    	$$self.$inject_state = $$props => {
    		if ('myPlayer' in $$props) $$invalidate(3, myPlayer = $$props.myPlayer);
    		if ('playerWidth' in $$props) $$invalidate(4, playerWidth = $$props.playerWidth);
    		if ('banners' in $$props) $$invalidate(0, banners = $$props.banners);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*banners*/ 1) {
    			if (bannerNames.every(name => banners[name]?.isOpen == false)) continuePlay();
    		}
    	};

    	return [banners, bannerNames, closeBanner, myPlayer, playerWidth, click_handler];
    }

    class SpamHell extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$k, create_fragment$k, safe_not_equal, { myPlayer: 3, playerWidth: 4 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "SpamHell",
    			options,
    			id: create_fragment$k.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*myPlayer*/ ctx[3] === undefined && !('myPlayer' in props)) {
    			console.warn("<SpamHell> was created without expected prop 'myPlayer'");
    		}

    		if (/*playerWidth*/ ctx[4] === undefined && !('playerWidth' in props)) {
    			console.warn("<SpamHell> was created without expected prop 'playerWidth'");
    		}
    	}

    	get myPlayer() {
    		throw new Error("<SpamHell>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set myPlayer(value) {
    		throw new Error("<SpamHell>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get playerWidth() {
    		throw new Error("<SpamHell>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set playerWidth(value) {
    		throw new Error("<SpamHell>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/Foodporn/Header.svelte generated by Svelte v3.42.4 */

    const file$j = "src/components/Foodporn/Header.svelte";

    function create_fragment$j(ctx) {
    	let div4;
    	let div1;
    	let div0;
    	let t;
    	let div3;
    	let div2;

    	const block = {
    		c: function create() {
    			div4 = element("div");
    			div1 = element("div");
    			div0 = element("div");
    			t = space();
    			div3 = element("div");
    			div2 = element("div");
    			attr_dev(div0, "class", "logo svelte-vf64mx");
    			add_location(div0, file$j, 1, 20, 41);
    			attr_dev(div1, "class", "left svelte-vf64mx");
    			add_location(div1, file$j, 1, 2, 23);
    			attr_dev(div2, "class", "text svelte-vf64mx");
    			add_location(div2, file$j, 3, 4, 94);
    			attr_dev(div3, "class", "right svelte-vf64mx");
    			add_location(div3, file$j, 2, 2, 70);
    			attr_dev(div4, "class", "header svelte-vf64mx");
    			add_location(div4, file$j, 0, 0, 0);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div4, anchor);
    			append_dev(div4, div1);
    			append_dev(div1, div0);
    			append_dev(div4, t);
    			append_dev(div4, div3);
    			append_dev(div3, div2);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div4);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$j.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$j($$self, $$props) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Header', slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Header> was created with unknown prop '${key}'`);
    	});

    	return [];
    }

    class Header$2 extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$j, create_fragment$j, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Header",
    			options,
    			id: create_fragment$j.name
    		});
    	}
    }

    /* src/components/Foodporn/Ingridient.svelte generated by Svelte v3.42.4 */

    const file$i = "src/components/Foodporn/Ingridient.svelte";

    // (11:8) {#if item.selected}
    function create_if_block$9(ctx) {
    	let svg;
    	let path;

    	const block = {
    		c: function create() {
    			svg = svg_element("svg");
    			path = svg_element("path");
    			attr_dev(path, "d", "M2.75293 12.7487L14.1926 22.675L27.7123 2.30002");
    			attr_dev(path, "stroke", "#070707");
    			attr_dev(path, "stroke-width", "8");
    			add_location(path, file$i, 12, 12, 452);
    			attr_dev(svg, "class", "checkbox svelte-7uyetf");
    			attr_dev(svg, "viewBox", "0 0 32 29");
    			attr_dev(svg, "fill", "none");
    			attr_dev(svg, "xmlns", "http://www.w3.org/2000/svg");
    			add_location(svg, file$i, 11, 10, 350);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, svg, anchor);
    			append_dev(svg, path);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(svg);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$9.name,
    		type: "if",
    		source: "(11:8) {#if item.selected}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$i(ctx) {
    	let div7;
    	let div3;
    	let div0;
    	let t0;
    	let div1;
    	let t1;
    	let div2;
    	let t2;
    	let div6;
    	let div4;
    	let div4_style_value;
    	let t3;
    	let div5;
    	let t4_value = /*item*/ ctx[1].text + "";
    	let t4;
    	let mounted;
    	let dispose;
    	let if_block = /*item*/ ctx[1].selected && create_if_block$9(ctx);

    	const block = {
    		c: function create() {
    			div7 = element("div");
    			div3 = element("div");
    			div0 = element("div");
    			t0 = space();
    			div1 = element("div");
    			if (if_block) if_block.c();
    			t1 = space();
    			div2 = element("div");
    			t2 = space();
    			div6 = element("div");
    			div4 = element("div");
    			t3 = space();
    			div5 = element("div");
    			t4 = text(t4_value);
    			attr_dev(div0, "class", "line svelte-7uyetf");
    			toggle_class(div0, "invisible", /*i*/ ctx[0] === 0);
    			add_location(div0, file$i, 8, 6, 207);
    			attr_dev(div1, "class", "circle svelte-7uyetf");
    			toggle_class(div1, "selected", /*item*/ ctx[1].selected);
    			add_location(div1, file$i, 9, 6, 260);
    			attr_dev(div2, "class", "line svelte-7uyetf");
    			toggle_class(div2, "invisible", /*i*/ ctx[0] === /*ingridients*/ ctx[3].length - 1);
    			add_location(div2, file$i, 16, 6, 597);
    			attr_dev(div3, "class", "leftLine svelte-7uyetf");
    			add_location(div3, file$i, 7, 4, 178);
    			attr_dev(div4, "class", "ingridient svelte-7uyetf");
    			attr_dev(div4, "style", div4_style_value = `background-image: url(./images/burger/${/*item*/ ctx[1].name}.png)`);
    			add_location(div4, file$i, 19, 6, 713);
    			attr_dev(div5, "class", "text svelte-7uyetf");
    			add_location(div5, file$i, 20, 6, 812);
    			attr_dev(div6, "class", "imageWrapper svelte-7uyetf");
    			add_location(div6, file$i, 18, 4, 680);
    			attr_dev(div7, "class", "item svelte-7uyetf");
    			add_location(div7, file$i, 6, 0, 124);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div7, anchor);
    			append_dev(div7, div3);
    			append_dev(div3, div0);
    			append_dev(div3, t0);
    			append_dev(div3, div1);
    			if (if_block) if_block.m(div1, null);
    			append_dev(div3, t1);
    			append_dev(div3, div2);
    			append_dev(div7, t2);
    			append_dev(div7, div6);
    			append_dev(div6, div4);
    			append_dev(div6, t3);
    			append_dev(div6, div5);
    			append_dev(div5, t4);

    			if (!mounted) {
    				dispose = listen_dev(div7, "click", /*click_handler*/ ctx[4], false, false, false);
    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*i*/ 1) {
    				toggle_class(div0, "invisible", /*i*/ ctx[0] === 0);
    			}

    			if (/*item*/ ctx[1].selected) {
    				if (if_block) ; else {
    					if_block = create_if_block$9(ctx);
    					if_block.c();
    					if_block.m(div1, null);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}

    			if (dirty & /*item*/ 2) {
    				toggle_class(div1, "selected", /*item*/ ctx[1].selected);
    			}

    			if (dirty & /*i, ingridients*/ 9) {
    				toggle_class(div2, "invisible", /*i*/ ctx[0] === /*ingridients*/ ctx[3].length - 1);
    			}

    			if (dirty & /*item*/ 2 && div4_style_value !== (div4_style_value = `background-image: url(./images/burger/${/*item*/ ctx[1].name}.png)`)) {
    				attr_dev(div4, "style", div4_style_value);
    			}

    			if (dirty & /*item*/ 2 && t4_value !== (t4_value = /*item*/ ctx[1].text + "")) set_data_dev(t4, t4_value);
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div7);
    			if (if_block) if_block.d();
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$i.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$i($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Ingridient', slots, []);
    	let { i = 0 } = $$props;
    	let { item } = $$props;

    	let { onClick = () => {
    		
    	} } = $$props;

    	let { ingridients = [] } = $$props;
    	const writable_props = ['i', 'item', 'onClick', 'ingridients'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Ingridient> was created with unknown prop '${key}'`);
    	});

    	const click_handler = () => onClick(item);

    	$$self.$$set = $$props => {
    		if ('i' in $$props) $$invalidate(0, i = $$props.i);
    		if ('item' in $$props) $$invalidate(1, item = $$props.item);
    		if ('onClick' in $$props) $$invalidate(2, onClick = $$props.onClick);
    		if ('ingridients' in $$props) $$invalidate(3, ingridients = $$props.ingridients);
    	};

    	$$self.$capture_state = () => ({ i, item, onClick, ingridients });

    	$$self.$inject_state = $$props => {
    		if ('i' in $$props) $$invalidate(0, i = $$props.i);
    		if ('item' in $$props) $$invalidate(1, item = $$props.item);
    		if ('onClick' in $$props) $$invalidate(2, onClick = $$props.onClick);
    		if ('ingridients' in $$props) $$invalidate(3, ingridients = $$props.ingridients);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [i, item, onClick, ingridients, click_handler];
    }

    class Ingridient extends SvelteComponentDev {
    	constructor(options) {
    		super(options);

    		init(this, options, instance$i, create_fragment$i, safe_not_equal, {
    			i: 0,
    			item: 1,
    			onClick: 2,
    			ingridients: 3
    		});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Ingridient",
    			options,
    			id: create_fragment$i.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*item*/ ctx[1] === undefined && !('item' in props)) {
    			console.warn("<Ingridient> was created without expected prop 'item'");
    		}
    	}

    	get i() {
    		throw new Error("<Ingridient>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set i(value) {
    		throw new Error("<Ingridient>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get item() {
    		throw new Error("<Ingridient>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set item(value) {
    		throw new Error("<Ingridient>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get onClick() {
    		throw new Error("<Ingridient>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set onClick(value) {
    		throw new Error("<Ingridient>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get ingridients() {
    		throw new Error("<Ingridient>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set ingridients(value) {
    		throw new Error("<Ingridient>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/Foodporn/Main.svelte generated by Svelte v3.42.4 */
    const file$h = "src/components/Foodporn/Main.svelte";

    function get_each_context$5(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[8] = list[i];
    	child_ctx[10] = i;
    	return child_ctx;
    }

    function get_each_context_1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[8] = list[i];
    	child_ctx[10] = i;
    	return child_ctx;
    }

    // (33:6) {#each selected as item, i}
    function create_each_block_1(ctx) {
    	let div1;
    	let div0;
    	let div0_style_value;

    	const block = {
    		c: function create() {
    			div1 = element("div");
    			div0 = element("div");
    			attr_dev(div0, "class", "ingridient svelte-1j5hjii");
    			attr_dev(div0, "style", div0_style_value = `background-image: url(./images/burger/${/*item*/ ctx[8].name}.png)`);
    			add_location(div0, file$h, 37, 10, 1268);
    			attr_dev(div1, "class", "ingridientWrapper svelte-1j5hjii");
    			attr_dev(div1, "style", `z-index:${100 - /*i*/ ctx[10]}; transform:translateX(${/*i*/ ctx[10] % 2 == 0 ? -2 : 2}%);`);
    			add_location(div1, file$h, 33, 8, 1120);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div1, anchor);
    			append_dev(div1, div0);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*selected*/ 16 && div0_style_value !== (div0_style_value = `background-image: url(./images/burger/${/*item*/ ctx[8].name}.png)`)) {
    				attr_dev(div0, "style", div0_style_value);
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div1);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block_1.name,
    		type: "each",
    		source: "(33:6) {#each selected as item, i}",
    		ctx
    	});

    	return block;
    }

    // (51:6) {#each ingridients as item, i}
    function create_each_block$5(ctx) {
    	let ingridient;
    	let current;

    	ingridient = new Ingridient({
    			props: {
    				i: /*i*/ ctx[10],
    				item: /*item*/ ctx[8],
    				onClick: /*toggle*/ ctx[5],
    				ingridients: /*ingridients*/ ctx[1]
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(ingridient.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(ingridient, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const ingridient_changes = {};
    			if (dirty & /*ingridients*/ 2) ingridient_changes.item = /*item*/ ctx[8];
    			if (dirty & /*ingridients*/ 2) ingridient_changes.ingridients = /*ingridients*/ ctx[1];
    			ingridient.$set(ingridient_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(ingridient.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(ingridient.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(ingridient, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$5.name,
    		type: "each",
    		source: "(51:6) {#each ingridients as item, i}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$h(ctx) {
    	let div10;
    	let div5;
    	let div4;
    	let div1;
    	let div0;
    	let t0;
    	let t1;
    	let div3;
    	let div2;
    	let t2;
    	let button;
    	let t4;
    	let div9;
    	let div6;
    	let div6_style_value;
    	let t5;
    	let div7;
    	let t6;
    	let div8;
    	let current;
    	let mounted;
    	let dispose;
    	let each_value_1 = /*selected*/ ctx[4];
    	validate_each_argument(each_value_1);
    	let each_blocks_1 = [];

    	for (let i = 0; i < each_value_1.length; i += 1) {
    		each_blocks_1[i] = create_each_block_1(get_each_context_1(ctx, each_value_1, i));
    	}

    	let each_value = /*ingridients*/ ctx[1];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$5(get_each_context$5(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	const block = {
    		c: function create() {
    			div10 = element("div");
    			div5 = element("div");
    			div4 = element("div");
    			div1 = element("div");
    			div0 = element("div");
    			t0 = space();

    			for (let i = 0; i < each_blocks_1.length; i += 1) {
    				each_blocks_1[i].c();
    			}

    			t1 = space();
    			div3 = element("div");
    			div2 = element("div");
    			t2 = space();
    			button = element("button");
    			button.textContent = "Заказать";
    			t4 = space();
    			div9 = element("div");
    			div6 = element("div");
    			t5 = space();
    			div7 = element("div");
    			t6 = space();
    			div8 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(div0, "class", "burgeringridient svelte-1j5hjii");
    			attr_dev(div0, "style", `background-image: url(./images/burger/topburger.png)`);
    			add_location(div0, file$h, 30, 8, 969);
    			set_style(div1, "z-index", "101");
    			attr_dev(div1, "class", "burgeringridientWrapper svelte-1j5hjii");
    			add_location(div1, file$h, 29, 6, 902);
    			attr_dev(div2, "class", "burgeringridient svelte-1j5hjii");
    			attr_dev(div2, "style", `background-image: url(./images/burger/bottomburger.png)`);
    			add_location(div2, file$h, 41, 8, 1442);
    			attr_dev(div3, "class", "burgeringridientWrapper svelte-1j5hjii");
    			add_location(div3, file$h, 40, 6, 1396);
    			attr_dev(button, "class", "order svelte-1j5hjii");
    			add_location(button, file$h, 43, 6, 1560);
    			attr_dev(div4, "class", "burger svelte-1j5hjii");
    			add_location(div4, file$h, 28, 4, 875);
    			attr_dev(div5, "class", "left svelte-1j5hjii");
    			add_location(div5, file$h, 27, 2, 852);
    			attr_dev(div6, "class", "topShadow svelte-1j5hjii");

    			attr_dev(div6, "style", div6_style_value = `opacity: ${/*currentScroll*/ ctx[2] > 50
			? 1
			: /*currentScroll*/ ctx[2] / 100 * 2}`);

    			add_location(div6, file$h, 47, 4, 1670);
    			attr_dev(div7, "class", "bottomShadow svelte-1j5hjii");
    			add_location(div7, file$h, 48, 4, 1773);
    			attr_dev(div8, "class", "ingridients svelte-1j5hjii");
    			add_location(div8, file$h, 49, 4, 1806);
    			attr_dev(div9, "class", "right svelte-1j5hjii");
    			add_location(div9, file$h, 46, 2, 1646);
    			attr_dev(div10, "class", "main svelte-1j5hjii");
    			add_location(div10, file$h, 26, 0, 831);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div10, anchor);
    			append_dev(div10, div5);
    			append_dev(div5, div4);
    			append_dev(div4, div1);
    			append_dev(div1, div0);
    			append_dev(div4, t0);

    			for (let i = 0; i < each_blocks_1.length; i += 1) {
    				each_blocks_1[i].m(div4, null);
    			}

    			append_dev(div4, t1);
    			append_dev(div4, div3);
    			append_dev(div3, div2);
    			append_dev(div4, t2);
    			append_dev(div4, button);
    			append_dev(div10, t4);
    			append_dev(div10, div9);
    			append_dev(div9, div6);
    			append_dev(div9, t5);
    			append_dev(div9, div7);
    			append_dev(div9, t6);
    			append_dev(div9, div8);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div8, null);
    			}

    			/*div8_binding*/ ctx[7](div8);
    			current = true;

    			if (!mounted) {
    				dispose = [
    					listen_dev(
    						button,
    						"click",
    						function () {
    							if (is_function(/*continuePlay*/ ctx[0])) /*continuePlay*/ ctx[0].apply(this, arguments);
    						},
    						false,
    						false,
    						false
    					),
    					listen_dev(div8, "scroll", /*onScroll*/ ctx[6], false, false, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(new_ctx, [dirty]) {
    			ctx = new_ctx;

    			if (dirty & /*selected*/ 16) {
    				each_value_1 = /*selected*/ ctx[4];
    				validate_each_argument(each_value_1);
    				let i;

    				for (i = 0; i < each_value_1.length; i += 1) {
    					const child_ctx = get_each_context_1(ctx, each_value_1, i);

    					if (each_blocks_1[i]) {
    						each_blocks_1[i].p(child_ctx, dirty);
    					} else {
    						each_blocks_1[i] = create_each_block_1(child_ctx);
    						each_blocks_1[i].c();
    						each_blocks_1[i].m(div4, t1);
    					}
    				}

    				for (; i < each_blocks_1.length; i += 1) {
    					each_blocks_1[i].d(1);
    				}

    				each_blocks_1.length = each_value_1.length;
    			}

    			if (!current || dirty & /*currentScroll*/ 4 && div6_style_value !== (div6_style_value = `opacity: ${/*currentScroll*/ ctx[2] > 50
			? 1
			: /*currentScroll*/ ctx[2] / 100 * 2}`)) {
    				attr_dev(div6, "style", div6_style_value);
    			}

    			if (dirty & /*ingridients, toggle*/ 34) {
    				each_value = /*ingridients*/ ctx[1];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$5(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block$5(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div8, null);
    					}
    				}

    				group_outros();

    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div10);
    			destroy_each(each_blocks_1, detaching);
    			destroy_each(each_blocks, detaching);
    			/*div8_binding*/ ctx[7](null);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$h.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$h($$self, $$props, $$invalidate) {
    	let selected;
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Main', slots, []);
    	let { continuePlay } = $$props;

    	let ingridients = [
    		{ name: 'beacon', text: 'Бинджвотч-бекон' },
    		{
    			name: 'tomato',
    			text: 'Помидорка Stories'
    		},
    		{ name: 'asmr', text: 'ASMR' },
    		{ name: 'tiktok', text: 'Тик-ток котлета' },
    		{
    			name: 'tiktokcats',
    			text: 'Тик-ток котлета c котиками'
    		},
    		{ name: 'onion', text: 'Видео-лук' },
    		{ name: 'cheese', text: 'Науч-поп сыр' },
    		{
    			name: 'salad',
    			text: '10-ти часовой салат'
    		},
    		{
    			name: 'cucumba',
    			text: 'Интересный огуречек'
    		}
    	];

    	const toggle = item => {
    		item.selected = !item.selected;
    		$$invalidate(1, ingridients);
    	};

    	let currentScroll = 0;
    	let scrollBox;

    	const onScroll = () => {
    		$$invalidate(2, currentScroll = scrollBox.scrollTop);
    	};

    	const writable_props = ['continuePlay'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Main> was created with unknown prop '${key}'`);
    	});

    	function div8_binding($$value) {
    		binding_callbacks[$$value ? 'unshift' : 'push'](() => {
    			scrollBox = $$value;
    			$$invalidate(3, scrollBox);
    		});
    	}

    	$$self.$$set = $$props => {
    		if ('continuePlay' in $$props) $$invalidate(0, continuePlay = $$props.continuePlay);
    	};

    	$$self.$capture_state = () => ({
    		Ingridient,
    		continuePlay,
    		ingridients,
    		toggle,
    		currentScroll,
    		scrollBox,
    		onScroll,
    		selected
    	});

    	$$self.$inject_state = $$props => {
    		if ('continuePlay' in $$props) $$invalidate(0, continuePlay = $$props.continuePlay);
    		if ('ingridients' in $$props) $$invalidate(1, ingridients = $$props.ingridients);
    		if ('currentScroll' in $$props) $$invalidate(2, currentScroll = $$props.currentScroll);
    		if ('scrollBox' in $$props) $$invalidate(3, scrollBox = $$props.scrollBox);
    		if ('selected' in $$props) $$invalidate(4, selected = $$props.selected);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*ingridients*/ 2) {
    			$$invalidate(4, selected = ingridients.filter(item => item.selected));
    		}
    	};

    	return [
    		continuePlay,
    		ingridients,
    		currentScroll,
    		scrollBox,
    		selected,
    		toggle,
    		onScroll,
    		div8_binding
    	];
    }

    class Main extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$h, create_fragment$h, safe_not_equal, { continuePlay: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Main",
    			options,
    			id: create_fragment$h.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*continuePlay*/ ctx[0] === undefined && !('continuePlay' in props)) {
    			console.warn("<Main> was created without expected prop 'continuePlay'");
    		}
    	}

    	get continuePlay() {
    		throw new Error("<Main>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set continuePlay(value) {
    		throw new Error("<Main>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/Foodporn.svelte generated by Svelte v3.42.4 */
    const file$g = "src/components/Foodporn.svelte";

    // (16:0) {#if visible}
    function create_if_block$8(ctx) {
    	let div1;
    	let header;
    	let t0;
    	let div0;
    	let t1;
    	let main;
    	let current;
    	header = new Header$2({ $$inline: true });

    	main = new Main({
    			props: { continuePlay: /*continuePlay*/ ctx[1] },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div1 = element("div");
    			create_component(header.$$.fragment);
    			t0 = space();
    			div0 = element("div");
    			t1 = space();
    			create_component(main.$$.fragment);
    			attr_dev(div0, "class", "spacer svelte-1daaro9");
    			add_location(div0, file$g, 18, 4, 368);
    			attr_dev(div1, "class", "container svelte-1daaro9");
    			add_location(div1, file$g, 16, 2, 325);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div1, anchor);
    			mount_component(header, div1, null);
    			append_dev(div1, t0);
    			append_dev(div1, div0);
    			append_dev(div1, t1);
    			mount_component(main, div1, null);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(header.$$.fragment, local);
    			transition_in(main.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(header.$$.fragment, local);
    			transition_out(main.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div1);
    			destroy_component(header);
    			destroy_component(main);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$8.name,
    		type: "if",
    		source: "(16:0) {#if visible}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$g(ctx) {
    	let if_block_anchor;
    	let current;
    	let if_block = /*visible*/ ctx[0] && create_if_block$8(ctx);

    	const block = {
    		c: function create() {
    			if (if_block) if_block.c();
    			if_block_anchor = empty();
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if (if_block) if_block.m(target, anchor);
    			insert_dev(target, if_block_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (/*visible*/ ctx[0]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);

    					if (dirty & /*visible*/ 1) {
    						transition_in(if_block, 1);
    					}
    				} else {
    					if_block = create_if_block$8(ctx);
    					if_block.c();
    					transition_in(if_block, 1);
    					if_block.m(if_block_anchor.parentNode, if_block_anchor);
    				}
    			} else if (if_block) {
    				group_outros();

    				transition_out(if_block, 1, 1, () => {
    					if_block = null;
    				});

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (if_block) if_block.d(detaching);
    			if (detaching) detach_dev(if_block_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$g.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$g($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Foodporn', slots, []);
    	let { myPlayer } = $$props;
    	let visible = true;

    	onMount(() => {
    		myPlayer?.pause();
    	});

    	const continuePlay = () => {
    		$$invalidate(0, visible = false);
    		myPlayer?.play();
    	};

    	const writable_props = ['myPlayer'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Foodporn> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('myPlayer' in $$props) $$invalidate(2, myPlayer = $$props.myPlayer);
    	};

    	$$self.$capture_state = () => ({
    		Header: Header$2,
    		Main,
    		onMount,
    		myPlayer,
    		visible,
    		continuePlay
    	});

    	$$self.$inject_state = $$props => {
    		if ('myPlayer' in $$props) $$invalidate(2, myPlayer = $$props.myPlayer);
    		if ('visible' in $$props) $$invalidate(0, visible = $$props.visible);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [visible, continuePlay, myPlayer];
    }

    class Foodporn extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$g, create_fragment$g, safe_not_equal, { myPlayer: 2 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Foodporn",
    			options,
    			id: create_fragment$g.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*myPlayer*/ ctx[2] === undefined && !('myPlayer' in props)) {
    			console.warn("<Foodporn> was created without expected prop 'myPlayer'");
    		}
    	}

    	get myPlayer() {
    		throw new Error("<Foodporn>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set myPlayer(value) {
    		throw new Error("<Foodporn>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/InstagramScroll/index.svelte generated by Svelte v3.42.4 */
    const file$f = "src/components/InstagramScroll/index.svelte";

    function create_fragment$f(ctx) {
    	let div;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			div = element("div");
    			attr_dev(div, "class", "instagram svelte-y8hb3g");
    			add_location(div, file$f, 45, 0, 1104);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);

    			if (!mounted) {
    				dispose = [
    					listen_dev(div, "pointerdown", /*pointerStartHandler*/ ctx[0], false, false, false),
    					listen_dev(div, "pointerup", /*pointerEndHandler*/ ctx[1], false, false, false),
    					listen_dev(div, "touchstart", /*pointerStartHandler*/ ctx[0], false, false, false),
    					listen_dev(div, "touchend", /*pointerEndHandler*/ ctx[1], false, false, false)
    				];

    				mounted = true;
    			}
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$f.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$f($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('InstagramScroll', slots, []);
    	let { myPlayer } = $$props;

    	// export let playerWidth;
    	onMount(() => {
    		myPlayer.pause();
    		myPlayer.controls(false);
    	});

    	const continuePlay = () => {
    		myPlayer.play();
    		myPlayer.controls(true);
    	};

    	let coords = {};
    	const requiredDifference = myPlayer.height_ / 2;

    	const eventHandler = e => {
    		if (e instanceof TouchEvent && e.changedTouches.length === 1) {
    			return e.changedTouches[0];
    		} else if (e instanceof PointerEvent) {
    			return e;
    		} else return null;
    	};

    	const pointerStartHandler = e => {
    		const event = eventHandler(e);
    		if (!event) return;
    		coords = { pageX: event.pageX, pageY: event.pageY };
    	};

    	const pointerEndHandler = e => {
    		const event = eventHandler(e);
    		if (!event) return;

    		if (coords.pageY - event.pageY > requiredDifference && Math.abs(coords.pageX - event.pageX) < requiredDifference) {
    			continuePlay();
    		}
    	};

    	const writable_props = ['myPlayer'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<InstagramScroll> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('myPlayer' in $$props) $$invalidate(2, myPlayer = $$props.myPlayer);
    	};

    	$$self.$capture_state = () => ({
    		onMount,
    		myPlayer,
    		continuePlay,
    		coords,
    		requiredDifference,
    		eventHandler,
    		pointerStartHandler,
    		pointerEndHandler
    	});

    	$$self.$inject_state = $$props => {
    		if ('myPlayer' in $$props) $$invalidate(2, myPlayer = $$props.myPlayer);
    		if ('coords' in $$props) coords = $$props.coords;
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [pointerStartHandler, pointerEndHandler, myPlayer];
    }

    class InstagramScroll extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$f, create_fragment$f, safe_not_equal, { myPlayer: 2 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "InstagramScroll",
    			options,
    			id: create_fragment$f.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*myPlayer*/ ctx[2] === undefined && !('myPlayer' in props)) {
    			console.warn("<InstagramScroll> was created without expected prop 'myPlayer'");
    		}
    	}

    	get myPlayer() {
    		throw new Error("<InstagramScroll>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set myPlayer(value) {
    		throw new Error("<InstagramScroll>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/InstagramMask/Header.svelte generated by Svelte v3.42.4 */

    const file$e = "src/components/InstagramMask/Header.svelte";

    // (30:4) {:else}
    function create_else_block$3(ctx) {
    	let div0;
    	let img0;
    	let img0_src_value;
    	let t0;
    	let div1;
    	let img1;
    	let img1_src_value;
    	let t1;
    	let div2;
    	let img2;
    	let img2_src_value;

    	const block = {
    		c: function create() {
    			div0 = element("div");
    			img0 = element("img");
    			t0 = space();
    			div1 = element("div");
    			img1 = element("img");
    			t1 = space();
    			div2 = element("div");
    			img2 = element("img");
    			if (!src_url_equal(img0.src, img0_src_value = "./images/instagram/icons/options.png")) attr_dev(img0, "src", img0_src_value);
    			attr_dev(img0, "alt", "options icon");
    			attr_dev(img0, "class", "svelte-15c2b79");
    			add_location(img0, file$e, 31, 8, 1115);
    			attr_dev(div0, "class", "iconContainer svelte-15c2b79");
    			add_location(div0, file$e, 30, 4, 1079);
    			if (!src_url_equal(img1.src, img1_src_value = "./images/instagram/icons/flash-icon.png")) attr_dev(img1, "src", img1_src_value);
    			attr_dev(img1, "alt", "flash icon");
    			attr_dev(img1, "class", "svelte-15c2b79");
    			add_location(img1, file$e, 34, 8, 1236);
    			attr_dev(div1, "class", "iconContainer svelte-15c2b79");
    			add_location(div1, file$e, 33, 4, 1200);
    			if (!src_url_equal(img2.src, img2_src_value = "./images/instagram/icons/close.png")) attr_dev(img2, "src", img2_src_value);
    			attr_dev(img2, "alt", "close icon");
    			attr_dev(img2, "class", "svelte-15c2b79");
    			add_location(img2, file$e, 37, 8, 1358);
    			attr_dev(div2, "class", "iconContainer svelte-15c2b79");
    			add_location(div2, file$e, 36, 4, 1322);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div0, anchor);
    			append_dev(div0, img0);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, div1, anchor);
    			append_dev(div1, img1);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, div2, anchor);
    			append_dev(div2, img2);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div0);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(div1);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(div2);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$3.name,
    		type: "else",
    		source: "(30:4) {:else}",
    		ctx
    	});

    	return block;
    }

    // (6:4) {#if isReadyToPublish}
    function create_if_block$7(ctx) {
    	let div0;
    	let img0;
    	let img0_src_value;
    	let t0;
    	let div7;
    	let div1;
    	let img1;
    	let img1_src_value;
    	let t1;
    	let div2;
    	let img2;
    	let img2_src_value;
    	let t2;
    	let div3;
    	let img3;
    	let img3_src_value;
    	let t3;
    	let div4;
    	let img4;
    	let img4_src_value;
    	let t4;
    	let div5;
    	let img5;
    	let img5_src_value;
    	let t5;
    	let div6;
    	let img6;
    	let img6_src_value;

    	const block = {
    		c: function create() {
    			div0 = element("div");
    			img0 = element("img");
    			t0 = space();
    			div7 = element("div");
    			div1 = element("div");
    			img1 = element("img");
    			t1 = space();
    			div2 = element("div");
    			img2 = element("img");
    			t2 = space();
    			div3 = element("div");
    			img3 = element("img");
    			t3 = space();
    			div4 = element("div");
    			img4 = element("img");
    			t4 = space();
    			div5 = element("div");
    			img5 = element("img");
    			t5 = space();
    			div6 = element("div");
    			img6 = element("img");
    			if (!src_url_equal(img0.src, img0_src_value = "./images/instagram/icons/close.png")) attr_dev(img0, "src", img0_src_value);
    			attr_dev(img0, "alt", "close icon");
    			attr_dev(img0, "class", "svelte-15c2b79");
    			add_location(img0, file$e, 7, 12, 157);
    			attr_dev(div0, "class", "iconContainer svelte-15c2b79");
    			add_location(div0, file$e, 6, 8, 117);
    			if (!src_url_equal(img1.src, img1_src_value = "./images/instagram/icons/download.png")) attr_dev(img1, "src", img1_src_value);
    			attr_dev(img1, "alt", "download icon");
    			attr_dev(img1, "class", "svelte-15c2b79");
    			add_location(img1, file$e, 11, 12, 314);
    			attr_dev(div1, "class", "iconContainer svelte-15c2b79");
    			add_location(div1, file$e, 10, 8, 274);
    			if (!src_url_equal(img2.src, img2_src_value = "./images/instagram/icons/music.png")) attr_dev(img2, "src", img2_src_value);
    			attr_dev(img2, "alt", "music icon");
    			attr_dev(img2, "class", "svelte-15c2b79");
    			add_location(img2, file$e, 14, 12, 449);
    			attr_dev(div2, "class", "iconContainer svelte-15c2b79");
    			add_location(div2, file$e, 13, 8, 409);
    			if (!src_url_equal(img3.src, img3_src_value = "./images/instagram/icons/effects.png")) attr_dev(img3, "src", img3_src_value);
    			attr_dev(img3, "alt", "effects icon");
    			attr_dev(img3, "class", "svelte-15c2b79");
    			add_location(img3, file$e, 17, 12, 578);
    			attr_dev(div3, "class", "iconContainer svelte-15c2b79");
    			add_location(div3, file$e, 16, 8, 538);
    			if (!src_url_equal(img4.src, img4_src_value = "./images/instagram/icons/sticker.png")) attr_dev(img4, "src", img4_src_value);
    			attr_dev(img4, "alt", "sticker icon");
    			attr_dev(img4, "class", "svelte-15c2b79");
    			add_location(img4, file$e, 20, 12, 711);
    			attr_dev(div4, "class", "iconContainer svelte-15c2b79");
    			add_location(div4, file$e, 19, 8, 671);
    			if (!src_url_equal(img5.src, img5_src_value = "./images/instagram/icons/curve.png")) attr_dev(img5, "src", img5_src_value);
    			attr_dev(img5, "alt", "curve icon");
    			attr_dev(img5, "class", "svelte-15c2b79");
    			add_location(img5, file$e, 23, 12, 844);
    			attr_dev(div5, "class", "iconContainer svelte-15c2b79");
    			add_location(div5, file$e, 22, 8, 804);
    			if (!src_url_equal(img6.src, img6_src_value = "./images/instagram/icons/text.png")) attr_dev(img6, "src", img6_src_value);
    			attr_dev(img6, "alt", "text icon");
    			attr_dev(img6, "class", "svelte-15c2b79");
    			add_location(img6, file$e, 26, 12, 973);
    			attr_dev(div6, "class", "iconContainer svelte-15c2b79");
    			add_location(div6, file$e, 25, 8, 933);
    			attr_dev(div7, "class", "iconBlock svelte-15c2b79");
    			add_location(div7, file$e, 9, 4, 242);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div0, anchor);
    			append_dev(div0, img0);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, div7, anchor);
    			append_dev(div7, div1);
    			append_dev(div1, img1);
    			append_dev(div7, t1);
    			append_dev(div7, div2);
    			append_dev(div2, img2);
    			append_dev(div7, t2);
    			append_dev(div7, div3);
    			append_dev(div3, img3);
    			append_dev(div7, t3);
    			append_dev(div7, div4);
    			append_dev(div4, img4);
    			append_dev(div7, t4);
    			append_dev(div7, div5);
    			append_dev(div5, img5);
    			append_dev(div7, t5);
    			append_dev(div7, div6);
    			append_dev(div6, img6);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div0);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(div7);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$7.name,
    		type: "if",
    		source: "(6:4) {#if isReadyToPublish}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$e(ctx) {
    	let div;

    	function select_block_type(ctx, dirty) {
    		if (/*isReadyToPublish*/ ctx[0]) return create_if_block$7;
    		return create_else_block$3;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block = current_block_type(ctx);

    	const block = {
    		c: function create() {
    			div = element("div");
    			if_block.c();
    			attr_dev(div, "class", "header svelte-15c2b79");
    			add_location(div, file$e, 4, 0, 61);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			if_block.m(div, null);
    		},
    		p: function update(ctx, [dirty]) {
    			if (current_block_type !== (current_block_type = select_block_type(ctx))) {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(div, null);
    				}
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			if_block.d();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$e.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$e($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Header', slots, []);
    	let { isReadyToPublish = false } = $$props;
    	const writable_props = ['isReadyToPublish'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Header> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('isReadyToPublish' in $$props) $$invalidate(0, isReadyToPublish = $$props.isReadyToPublish);
    	};

    	$$self.$capture_state = () => ({ isReadyToPublish });

    	$$self.$inject_state = $$props => {
    		if ('isReadyToPublish' in $$props) $$invalidate(0, isReadyToPublish = $$props.isReadyToPublish);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [isReadyToPublish];
    }

    class Header$1 extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$e, create_fragment$e, safe_not_equal, { isReadyToPublish: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Header",
    			options,
    			id: create_fragment$e.name
    		});
    	}

    	get isReadyToPublish() {
    		throw new Error("<Header>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set isReadyToPublish(value) {
    		throw new Error("<Header>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/InstagramMask/Footer.svelte generated by Svelte v3.42.4 */
    const file$d = "src/components/InstagramMask/Footer.svelte";

    function get_each_context$4(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[18] = list[i];
    	child_ctx[20] = i;
    	return child_ctx;
    }

    // (64:4) {:else}
    function create_else_block$2(ctx) {
    	let div0;
    	let t0;
    	let div3;
    	let div1;
    	let img0;
    	let img0_src_value;
    	let t1;
    	let button;
    	let t3;
    	let div2;
    	let img1;
    	let img1_src_value;
    	let mounted;
    	let dispose;
    	let each_value = /*masks*/ ctx[7];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$4(get_each_context$4(ctx, each_value, i));
    	}

    	const block = {
    		c: function create() {
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t0 = space();
    			div3 = element("div");
    			div1 = element("div");
    			img0 = element("img");
    			t1 = space();
    			button = element("button");
    			button.textContent = "Готово";
    			t3 = space();
    			div2 = element("div");
    			img1 = element("img");
    			attr_dev(div0, "class", "masks svelte-pfbjbx");
    			set_style(div0, "padding-left", /*centerCoord*/ ctx[6] + "px");
    			set_style(div0, "margin-left", /*xOffset*/ ctx[5] + "px");
    			add_location(div0, file$d, 64, 8, 2057);
    			if (!src_url_equal(img0.src, img0_src_value = "./images/instagram//gallery-girl.png")) attr_dev(img0, "src", img0_src_value);
    			attr_dev(img0, "alt", "gallery");
    			attr_dev(img0, "class", "svelte-pfbjbx");
    			add_location(img0, file$d, 81, 16, 2751);
    			attr_dev(div1, "class", "iconContainer svelte-pfbjbx");
    			add_location(div1, file$d, 80, 12, 2707);
    			attr_dev(button, "class", "svelte-pfbjbx");
    			add_location(button, file$d, 83, 12, 2847);
    			if (!src_url_equal(img1.src, img1_src_value = "./images/instagram/icons/flip-camera.png")) attr_dev(img1, "src", img1_src_value);
    			attr_dev(img1, "alt", "flip");
    			attr_dev(img1, "class", "svelte-pfbjbx");
    			add_location(img1, file$d, 85, 16, 2958);
    			attr_dev(div2, "class", "iconContainer svelte-pfbjbx");
    			add_location(div2, file$d, 84, 12, 2914);
    			attr_dev(div3, "class", "actions svelte-pfbjbx");
    			add_location(div3, file$d, 79, 8, 2673);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div0, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div0, null);
    			}

    			insert_dev(target, t0, anchor);
    			insert_dev(target, div3, anchor);
    			append_dev(div3, div1);
    			append_dev(div1, img0);
    			append_dev(div3, t1);
    			append_dev(div3, button);
    			append_dev(div3, t3);
    			append_dev(div3, div2);
    			append_dev(div2, img1);

    			if (!mounted) {
    				dispose = [
    					listen_dev(div0, "touchstart", /*touchstart_handler*/ ctx[12], false, false, false),
    					listen_dev(div0, "mousedown", /*mousedown_handler*/ ctx[13], false, false, false),
    					listen_dev(div0, "mousewheel", /*onScroll*/ ctx[8], false, false, false),
    					listen_dev(div0, "touchmove", /*onScroll*/ ctx[8], false, false, false),
    					listen_dev(div0, "mousemove", /*onScroll*/ ctx[8], false, false, false),
    					listen_dev(
    						button,
    						"click",
    						function () {
    							if (is_function(/*setIsReadyToPublish*/ ctx[1])) /*setIsReadyToPublish*/ ctx[1].apply(this, arguments);
    						},
    						false,
    						false,
    						false
    					)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (dirty & /*setMaskActive, masks*/ 640) {
    				each_value = /*masks*/ ctx[7];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$4(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$4(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div0, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}

    			if (dirty & /*centerCoord*/ 64) {
    				set_style(div0, "padding-left", /*centerCoord*/ ctx[6] + "px");
    			}

    			if (dirty & /*xOffset*/ 32) {
    				set_style(div0, "margin-left", /*xOffset*/ ctx[5] + "px");
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div0);
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(div3);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$2.name,
    		type: "else",
    		source: "(64:4) {:else}",
    		ctx
    	});

    	return block;
    }

    // (58:4) {#if isReadyToPublish}
    function create_if_block$6(ctx) {
    	let div2;
    	let div0;
    	let t0;
    	let button;
    	let t2;
    	let div1;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			div2 = element("div");
    			div0 = element("div");
    			t0 = space();
    			button = element("button");
    			button.textContent = "Опубликовать";
    			t2 = space();
    			div1 = element("div");
    			add_location(div0, file$d, 59, 12, 1928);
    			attr_dev(button, "class", "svelte-pfbjbx");
    			add_location(button, file$d, 60, 12, 1948);
    			add_location(div1, file$d, 61, 12, 2014);
    			attr_dev(div2, "class", "actions svelte-pfbjbx");
    			add_location(div2, file$d, 58, 8, 1894);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div2, anchor);
    			append_dev(div2, div0);
    			append_dev(div2, t0);
    			append_dev(div2, button);
    			append_dev(div2, t2);
    			append_dev(div2, div1);

    			if (!mounted) {
    				dispose = listen_dev(
    					button,
    					"click",
    					function () {
    						if (is_function(/*continuePlay*/ ctx[2])) /*continuePlay*/ ctx[2].apply(this, arguments);
    					},
    					false,
    					false,
    					false
    				);

    				mounted = true;
    			}
    		},
    		p: function update(new_ctx, dirty) {
    			ctx = new_ctx;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div2);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$6.name,
    		type: "if",
    		source: "(58:4) {#if isReadyToPublish}",
    		ctx
    	});

    	return block;
    }

    // (74:12) {#each masks as mask, index}
    function create_each_block$4(ctx) {
    	let div;
    	let img;
    	let img_src_value;
    	let t;
    	let mounted;
    	let dispose;

    	function click_handler() {
    		return /*click_handler*/ ctx[11](/*index*/ ctx[20]);
    	}

    	const block = {
    		c: function create() {
    			div = element("div");
    			img = element("img");
    			t = space();
    			if (!src_url_equal(img.src, img_src_value = "./images/instagram/masks/" + /*mask*/ ctx[18])) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "alt", "mask");
    			attr_dev(img, "class", "svelte-pfbjbx");
    			add_location(img, file$d, 75, 20, 2550);
    			attr_dev(div, "class", "mask svelte-pfbjbx");
    			add_location(div, file$d, 74, 16, 2473);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, img);
    			append_dev(div, t);

    			if (!mounted) {
    				dispose = listen_dev(div, "click", click_handler, false, false, false);
    				mounted = true;
    			}
    		},
    		p: function update(new_ctx, dirty) {
    			ctx = new_ctx;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$4.name,
    		type: "each",
    		source: "(74:12) {#each masks as mask, index}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$d(ctx) {
    	let div;
    	let div_resize_listener;

    	function select_block_type(ctx, dirty) {
    		if (/*isReadyToPublish*/ ctx[0]) return create_if_block$6;
    		return create_else_block$2;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block = current_block_type(ctx);

    	const block = {
    		c: function create() {
    			div = element("div");
    			if_block.c();
    			attr_dev(div, "class", "footer svelte-pfbjbx");
    			add_render_callback(() => /*div_elementresize_handler*/ ctx[14].call(div));
    			add_location(div, file$d, 56, 0, 1804);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			if_block.m(div, null);
    			div_resize_listener = add_resize_listener(div, /*div_elementresize_handler*/ ctx[14].bind(div));
    		},
    		p: function update(ctx, [dirty]) {
    			if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
    				if_block.p(ctx, dirty);
    			} else {
    				if_block.d(1);
    				if_block = current_block_type(ctx);

    				if (if_block) {
    					if_block.c();
    					if_block.m(div, null);
    				}
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			if_block.d();
    			div_resize_listener();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$d.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$d($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Footer', slots, []);
    	let { isReadyToPublish = false } = $$props;

    	let { setIsReadyToPublish = () => {
    		
    	} } = $$props;

    	let { setBackgroundImage = () => {
    		
    	} } = $$props;

    	let { continuePlay = () => {
    		
    	} } = $$props;

    	const masks = ['0.png', '1.png', '2.png', '3.png', '4.png', '5.png', '6.png'];
    	let lastXcoord;

    	const moveHandler = e => {
    		if (e instanceof TouchEvent && e.changedTouches.length === 1) {
    			const deltaX = lastXcoord - e.changedTouches[0].clientX;
    			$$invalidate(3, lastXcoord = e.changedTouches[0].clientX);
    			return deltaX;
    		} else if (e instanceof MouseEvent && e.button === 0 && e.buttons === 1) {
    			const deltaX = lastXcoord - e.clientX;
    			$$invalidate(3, lastXcoord = e.clientX);
    			return deltaX;
    		} else if (e instanceof WheelEvent) {
    			return e.deltaX;
    		} else return 0;
    	};

    	let timerId;
    	let containerWidth = 0;
    	let xOffset = 0;
    	let centerCoord = 0;

    	onMount(() => {
    		$$invalidate(6, centerCoord = containerWidth * 0.5 - 40);
    	});

    	const onScroll = e => {
    		clearTimeout(timerId);
    		e.preventDefault();
    		const deltaX = moveHandler(e);
    		$$invalidate(5, xOffset -= deltaX);
    		if (xOffset > 0) $$invalidate(5, xOffset = 0);
    		const maxOffset = (masks.length - 1) * -90;
    		if (xOffset < maxOffset) $$invalidate(5, xOffset = maxOffset);
    		timerId = setTimeout(getActiveMask, 200);
    	};

    	const getActiveMask = () => {
    		const index = -1 * Math.floor((xOffset + 45) / 90);
    		setBackgroundImage(masks[index]);
    		$$invalidate(5, xOffset = -90 * index); // #TODO - make transition
    	};

    	const setMaskActive = index => {
    		setBackgroundImage(masks[index]);
    		$$invalidate(5, xOffset = -90 * index); // #TODO - make transition
    	};

    	const writable_props = [
    		'isReadyToPublish',
    		'setIsReadyToPublish',
    		'setBackgroundImage',
    		'continuePlay'
    	];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Footer> was created with unknown prop '${key}'`);
    	});

    	const click_handler = index => setMaskActive(index);
    	const touchstart_handler = e => $$invalidate(3, lastXcoord = e.changedTouches[0].clientX);
    	const mousedown_handler = e => $$invalidate(3, lastXcoord = e.clientX);

    	function div_elementresize_handler() {
    		containerWidth = this.clientWidth;
    		$$invalidate(4, containerWidth);
    	}

    	$$self.$$set = $$props => {
    		if ('isReadyToPublish' in $$props) $$invalidate(0, isReadyToPublish = $$props.isReadyToPublish);
    		if ('setIsReadyToPublish' in $$props) $$invalidate(1, setIsReadyToPublish = $$props.setIsReadyToPublish);
    		if ('setBackgroundImage' in $$props) $$invalidate(10, setBackgroundImage = $$props.setBackgroundImage);
    		if ('continuePlay' in $$props) $$invalidate(2, continuePlay = $$props.continuePlay);
    	};

    	$$self.$capture_state = () => ({
    		onMount,
    		isReadyToPublish,
    		setIsReadyToPublish,
    		setBackgroundImage,
    		continuePlay,
    		masks,
    		lastXcoord,
    		moveHandler,
    		timerId,
    		containerWidth,
    		xOffset,
    		centerCoord,
    		onScroll,
    		getActiveMask,
    		setMaskActive
    	});

    	$$self.$inject_state = $$props => {
    		if ('isReadyToPublish' in $$props) $$invalidate(0, isReadyToPublish = $$props.isReadyToPublish);
    		if ('setIsReadyToPublish' in $$props) $$invalidate(1, setIsReadyToPublish = $$props.setIsReadyToPublish);
    		if ('setBackgroundImage' in $$props) $$invalidate(10, setBackgroundImage = $$props.setBackgroundImage);
    		if ('continuePlay' in $$props) $$invalidate(2, continuePlay = $$props.continuePlay);
    		if ('lastXcoord' in $$props) $$invalidate(3, lastXcoord = $$props.lastXcoord);
    		if ('timerId' in $$props) timerId = $$props.timerId;
    		if ('containerWidth' in $$props) $$invalidate(4, containerWidth = $$props.containerWidth);
    		if ('xOffset' in $$props) $$invalidate(5, xOffset = $$props.xOffset);
    		if ('centerCoord' in $$props) $$invalidate(6, centerCoord = $$props.centerCoord);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [
    		isReadyToPublish,
    		setIsReadyToPublish,
    		continuePlay,
    		lastXcoord,
    		containerWidth,
    		xOffset,
    		centerCoord,
    		masks,
    		onScroll,
    		setMaskActive,
    		setBackgroundImage,
    		click_handler,
    		touchstart_handler,
    		mousedown_handler,
    		div_elementresize_handler
    	];
    }

    class Footer extends SvelteComponentDev {
    	constructor(options) {
    		super(options);

    		init(this, options, instance$d, create_fragment$d, safe_not_equal, {
    			isReadyToPublish: 0,
    			setIsReadyToPublish: 1,
    			setBackgroundImage: 10,
    			continuePlay: 2
    		});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Footer",
    			options,
    			id: create_fragment$d.name
    		});
    	}

    	get isReadyToPublish() {
    		throw new Error("<Footer>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set isReadyToPublish(value) {
    		throw new Error("<Footer>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get setIsReadyToPublish() {
    		throw new Error("<Footer>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set setIsReadyToPublish(value) {
    		throw new Error("<Footer>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get setBackgroundImage() {
    		throw new Error("<Footer>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set setBackgroundImage(value) {
    		throw new Error("<Footer>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get continuePlay() {
    		throw new Error("<Footer>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set continuePlay(value) {
    		throw new Error("<Footer>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/InstagramMask/CentralIcons.svelte generated by Svelte v3.42.4 */

    const file$c = "src/components/InstagramMask/CentralIcons.svelte";

    // (6:4) {#if !isReadyToPublish}
    function create_if_block$5(ctx) {
    	let div0;
    	let img0;
    	let img0_src_value;
    	let t0;
    	let div1;
    	let img1;
    	let img1_src_value;
    	let t1;
    	let div2;
    	let img2;
    	let img2_src_value;
    	let t2;
    	let div3;
    	let img3;
    	let img3_src_value;

    	const block = {
    		c: function create() {
    			div0 = element("div");
    			img0 = element("img");
    			t0 = space();
    			div1 = element("div");
    			img1 = element("img");
    			t1 = space();
    			div2 = element("div");
    			img2 = element("img");
    			t2 = space();
    			div3 = element("div");
    			img3 = element("img");
    			if (!src_url_equal(img0.src, img0_src_value = "./images/instagram/icons/text.png")) attr_dev(img0, "src", img0_src_value);
    			attr_dev(img0, "alt", "text icon");
    			attr_dev(img0, "class", "svelte-16mdwgt");
    			add_location(img0, file$c, 7, 12, 164);
    			attr_dev(div0, "class", "iconContainer svelte-16mdwgt");
    			add_location(div0, file$c, 6, 8, 124);
    			if (!src_url_equal(img1.src, img1_src_value = "./images/instagram/icons/meta.png")) attr_dev(img1, "src", img1_src_value);
    			attr_dev(img1, "alt", "meta icon");
    			attr_dev(img1, "class", "svelte-16mdwgt");
    			add_location(img1, file$c, 10, 12, 291);
    			attr_dev(div1, "class", "iconContainer svelte-16mdwgt");
    			add_location(div1, file$c, 9, 8, 251);
    			if (!src_url_equal(img2.src, img2_src_value = "./images/instagram/icons/grid.png")) attr_dev(img2, "src", img2_src_value);
    			attr_dev(img2, "alt", "grid icon");
    			attr_dev(img2, "class", "svelte-16mdwgt");
    			add_location(img2, file$c, 13, 12, 418);
    			attr_dev(div2, "class", "iconContainer svelte-16mdwgt");
    			add_location(div2, file$c, 12, 8, 378);
    			if (!src_url_equal(img3.src, img3_src_value = "./images/instagram/icons/chevrone.png")) attr_dev(img3, "src", img3_src_value);
    			attr_dev(img3, "alt", "chevrone icon");
    			attr_dev(img3, "class", "svelte-16mdwgt");
    			add_location(img3, file$c, 16, 12, 545);
    			attr_dev(div3, "class", "iconContainer svelte-16mdwgt");
    			add_location(div3, file$c, 15, 8, 505);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div0, anchor);
    			append_dev(div0, img0);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, div1, anchor);
    			append_dev(div1, img1);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, div2, anchor);
    			append_dev(div2, img2);
    			insert_dev(target, t2, anchor);
    			insert_dev(target, div3, anchor);
    			append_dev(div3, img3);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div0);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(div1);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(div2);
    			if (detaching) detach_dev(t2);
    			if (detaching) detach_dev(div3);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$5.name,
    		type: "if",
    		source: "(6:4) {#if !isReadyToPublish}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$c(ctx) {
    	let div;
    	let if_block = !/*isReadyToPublish*/ ctx[0] && create_if_block$5(ctx);

    	const block = {
    		c: function create() {
    			div = element("div");
    			if (if_block) if_block.c();
    			attr_dev(div, "class", "centralIcons svelte-16mdwgt");
    			add_location(div, file$c, 4, 0, 61);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			if (if_block) if_block.m(div, null);
    		},
    		p: function update(ctx, [dirty]) {
    			if (!/*isReadyToPublish*/ ctx[0]) {
    				if (if_block) ; else {
    					if_block = create_if_block$5(ctx);
    					if_block.c();
    					if_block.m(div, null);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			if (if_block) if_block.d();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$c.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$c($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('CentralIcons', slots, []);
    	let { isReadyToPublish = false } = $$props;
    	const writable_props = ['isReadyToPublish'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<CentralIcons> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('isReadyToPublish' in $$props) $$invalidate(0, isReadyToPublish = $$props.isReadyToPublish);
    	};

    	$$self.$capture_state = () => ({ isReadyToPublish });

    	$$self.$inject_state = $$props => {
    		if ('isReadyToPublish' in $$props) $$invalidate(0, isReadyToPublish = $$props.isReadyToPublish);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [isReadyToPublish];
    }

    class CentralIcons extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$c, create_fragment$c, safe_not_equal, { isReadyToPublish: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "CentralIcons",
    			options,
    			id: create_fragment$c.name
    		});
    	}

    	get isReadyToPublish() {
    		throw new Error("<CentralIcons>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set isReadyToPublish(value) {
    		throw new Error("<CentralIcons>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/InstagramMask/index.svelte generated by Svelte v3.42.4 */
    const file$b = "src/components/InstagramMask/index.svelte";

    function create_fragment$b(ctx) {
    	let div1;
    	let div0;
    	let header;
    	let t0;
    	let centralicons;
    	let t1;
    	let footer;
    	let current;

    	header = new Header$1({
    			props: {
    				isReadyToPublish: /*isReadyToPublish*/ ctx[0]
    			},
    			$$inline: true
    		});

    	centralicons = new CentralIcons({
    			props: {
    				isReadyToPublish: /*isReadyToPublish*/ ctx[0]
    			},
    			$$inline: true
    		});

    	footer = new Footer({
    			props: {
    				isReadyToPublish: /*isReadyToPublish*/ ctx[0],
    				setIsReadyToPublish: /*setIsReadyToPublish*/ ctx[2],
    				backgroundImage: /*backgroundImage*/ ctx[1],
    				setBackgroundImage: /*setBackgroundImage*/ ctx[3],
    				continuePlay: /*continuePlay*/ ctx[4]
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div1 = element("div");
    			div0 = element("div");
    			create_component(header.$$.fragment);
    			t0 = space();
    			create_component(centralicons.$$.fragment);
    			t1 = space();
    			create_component(footer.$$.fragment);
    			attr_dev(div0, "class", "phoneContainer svelte-16ttuaw");
    			set_style(div0, "background-image", "url(./images/instagram/photos/" + /*backgroundImage*/ ctx[1] + ")");
    			add_location(div0, file$b, 28, 4, 615);
    			attr_dev(div1, "class", "background svelte-16ttuaw");
    			add_location(div1, file$b, 27, 0, 586);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div1, anchor);
    			append_dev(div1, div0);
    			mount_component(header, div0, null);
    			append_dev(div0, t0);
    			mount_component(centralicons, div0, null);
    			append_dev(div0, t1);
    			mount_component(footer, div0, null);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			const header_changes = {};
    			if (dirty & /*isReadyToPublish*/ 1) header_changes.isReadyToPublish = /*isReadyToPublish*/ ctx[0];
    			header.$set(header_changes);
    			const centralicons_changes = {};
    			if (dirty & /*isReadyToPublish*/ 1) centralicons_changes.isReadyToPublish = /*isReadyToPublish*/ ctx[0];
    			centralicons.$set(centralicons_changes);
    			const footer_changes = {};
    			if (dirty & /*isReadyToPublish*/ 1) footer_changes.isReadyToPublish = /*isReadyToPublish*/ ctx[0];
    			if (dirty & /*backgroundImage*/ 2) footer_changes.backgroundImage = /*backgroundImage*/ ctx[1];
    			footer.$set(footer_changes);

    			if (!current || dirty & /*backgroundImage*/ 2) {
    				set_style(div0, "background-image", "url(./images/instagram/photos/" + /*backgroundImage*/ ctx[1] + ")");
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(header.$$.fragment, local);
    			transition_in(centralicons.$$.fragment, local);
    			transition_in(footer.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(header.$$.fragment, local);
    			transition_out(centralicons.$$.fragment, local);
    			transition_out(footer.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div1);
    			destroy_component(header);
    			destroy_component(centralicons);
    			destroy_component(footer);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$b.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$b($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('InstagramMask', slots, []);
    	let { item } = $$props;
    	let { myPlayer } = $$props;
    	let { playerWidth } = $$props;
    	let isReadyToPublish = false;
    	const setIsReadyToPublish = () => $$invalidate(0, isReadyToPublish = true);
    	let backgroundImage = '0.png';
    	const setBackgroundImage = img => $$invalidate(1, backgroundImage = img);

    	onMount(() => {
    		myPlayer.pause();
    	});

    	const continuePlay = () => {
    		myPlayer.play();
    	};

    	const writable_props = ['item', 'myPlayer', 'playerWidth'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<InstagramMask> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('item' in $$props) $$invalidate(5, item = $$props.item);
    		if ('myPlayer' in $$props) $$invalidate(6, myPlayer = $$props.myPlayer);
    		if ('playerWidth' in $$props) $$invalidate(7, playerWidth = $$props.playerWidth);
    	};

    	$$self.$capture_state = () => ({
    		onMount,
    		Header: Header$1,
    		Footer,
    		CentralIcons,
    		item,
    		myPlayer,
    		playerWidth,
    		isReadyToPublish,
    		setIsReadyToPublish,
    		backgroundImage,
    		setBackgroundImage,
    		continuePlay
    	});

    	$$self.$inject_state = $$props => {
    		if ('item' in $$props) $$invalidate(5, item = $$props.item);
    		if ('myPlayer' in $$props) $$invalidate(6, myPlayer = $$props.myPlayer);
    		if ('playerWidth' in $$props) $$invalidate(7, playerWidth = $$props.playerWidth);
    		if ('isReadyToPublish' in $$props) $$invalidate(0, isReadyToPublish = $$props.isReadyToPublish);
    		if ('backgroundImage' in $$props) $$invalidate(1, backgroundImage = $$props.backgroundImage);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [
    		isReadyToPublish,
    		backgroundImage,
    		setIsReadyToPublish,
    		setBackgroundImage,
    		continuePlay,
    		item,
    		myPlayer,
    		playerWidth
    	];
    }

    class InstagramMask extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$b, create_fragment$b, safe_not_equal, { item: 5, myPlayer: 6, playerWidth: 7 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "InstagramMask",
    			options,
    			id: create_fragment$b.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*item*/ ctx[5] === undefined && !('item' in props)) {
    			console.warn("<InstagramMask> was created without expected prop 'item'");
    		}

    		if (/*myPlayer*/ ctx[6] === undefined && !('myPlayer' in props)) {
    			console.warn("<InstagramMask> was created without expected prop 'myPlayer'");
    		}

    		if (/*playerWidth*/ ctx[7] === undefined && !('playerWidth' in props)) {
    			console.warn("<InstagramMask> was created without expected prop 'playerWidth'");
    		}
    	}

    	get item() {
    		throw new Error("<InstagramMask>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set item(value) {
    		throw new Error("<InstagramMask>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get myPlayer() {
    		throw new Error("<InstagramMask>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set myPlayer(value) {
    		throw new Error("<InstagramMask>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get playerWidth() {
    		throw new Error("<InstagramMask>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set playerWidth(value) {
    		throw new Error("<InstagramMask>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/DamnbookFriends/Logo.svelte generated by Svelte v3.42.4 */

    const file$a = "src/components/DamnbookFriends/Logo.svelte";

    function create_fragment$a(ctx) {
    	let svg;
    	let rect;
    	let path0;
    	let path1;
    	let defs;
    	let linearGradient;
    	let stop0;
    	let stop1;

    	const block = {
    		c: function create() {
    			svg = svg_element("svg");
    			rect = svg_element("rect");
    			path0 = svg_element("path");
    			path1 = svg_element("path");
    			defs = svg_element("defs");
    			linearGradient = svg_element("linearGradient");
    			stop0 = svg_element("stop");
    			stop1 = svg_element("stop");
    			attr_dev(rect, "width", "41");
    			attr_dev(rect, "height", "41");
    			attr_dev(rect, "rx", "20.5");
    			attr_dev(rect, "fill", "url(#paint0_linear_117:836)");
    			add_location(rect, file$a, 7, 4, 123);
    			attr_dev(path0, "d", "M30.5214 19.524C30.5214 13.9225 25.7571 9 20.3357 9C14.75 9 9.98571 13.9225 9.98571 19.524C9.98571 22.0701 9 22.0701 9 24.4465C9 26.9926 12.7786 27.5018 14.4214 27.5018V27.6716C14.4214 29.0295 14.9143 30.3875 15.9 31.4059C16.7214 32.2546 16.8857 32.0849 17.05 31.5756C17.5429 30.3875 19.1857 30.048 20.5 30.048C21.8143 30.048 23.2929 30.2177 23.95 31.5756C24.1143 31.9151 24.4429 32.0849 25.1 31.4059C26.0857 30.3875 26.5786 29.0295 26.5786 27.6716V27.3321C28.0571 27.3321 32 26.8229 32 24.2768C31.6714 22.0701 30.5214 22.0701 30.5214 19.524ZM14.75 24.4465C12.7786 24.4465 11.3 23.2583 11.3 21.7306C11.3 19.0148 14.5857 17.6568 16.3929 17.6568C18.2 17.6568 19.5143 17.9963 19.1857 20.3727C18.8571 21.7306 16.5571 24.4465 14.75 24.4465ZM21.65 26.3137C21.1571 25.8044 20.6643 25.8044 20.3357 25.8044C20.0071 25.8044 19.5143 25.8044 19.0214 26.3137C18.6929 26.6531 18.3643 26.4834 18.3643 25.8044C18.2 24.6162 19.1857 22.5793 20.3357 22.5793C21.4857 22.5793 22.4714 24.6162 22.4714 25.8044C22.3071 26.4834 21.9786 26.6531 21.65 26.3137ZM25.9214 24.4465C24.1143 24.4465 21.65 21.9004 21.4857 20.203C21.1571 17.8266 22.3071 17.4871 24.2786 17.4871C26.25 17.4871 29.3714 18.845 29.3714 21.5609C29.3714 23.0886 27.7286 24.4465 25.9214 24.4465Z");
    			attr_dev(path0, "fill", "white");
    			add_location(path0, file$a, 8, 4, 204);
    			attr_dev(path1, "d", "M58.9668 23.958C58.9668 21.4873 59.5186 19.5195 60.6221 18.0547C61.7354 16.5898 63.2539 15.8574 65.1777 15.8574C66.7207 15.8574 67.9951 16.4336 69.001 17.5859V9.5H73.249V32H69.4258L69.2207 30.3154C68.166 31.6338 66.8086 32.293 65.1484 32.293C63.2832 32.293 61.7842 31.5605 60.6514 30.0957C59.5283 28.6211 58.9668 26.5752 58.9668 23.958ZM63.2002 24.2656C63.2002 25.75 63.459 26.8877 63.9766 27.6787C64.4941 28.4697 65.2461 28.8652 66.2324 28.8652C67.541 28.8652 68.4639 28.3135 69.001 27.21V20.9551C68.4736 19.8516 67.5605 19.2998 66.2617 19.2998C64.2207 19.2998 63.2002 20.9551 63.2002 24.2656ZM85.7148 32C85.5195 31.6191 85.3779 31.1455 85.29 30.5791C84.2646 31.7217 82.9316 32.293 81.291 32.293C79.7383 32.293 78.4492 31.8438 77.4238 30.9453C76.4082 30.0469 75.9004 28.9141 75.9004 27.5469C75.9004 25.8672 76.5205 24.5781 77.7607 23.6797C79.0107 22.7812 80.8125 22.3271 83.166 22.3174H85.1143V21.4092C85.1143 20.6768 84.9238 20.0908 84.543 19.6514C84.1719 19.2119 83.5811 18.9922 82.7705 18.9922C82.0576 18.9922 81.4961 19.1631 81.0859 19.5049C80.6855 19.8467 80.4854 20.3154 80.4854 20.9111H76.252C76.252 19.9932 76.5352 19.1436 77.1016 18.3623C77.668 17.5811 78.4688 16.9707 79.5039 16.5312C80.5391 16.082 81.7012 15.8574 82.9902 15.8574C84.9434 15.8574 86.4912 16.3506 87.6338 17.3369C88.7861 18.3135 89.3623 19.6904 89.3623 21.4678V28.3379C89.3721 29.8418 89.582 30.9795 89.9922 31.751V32H85.7148ZM82.2139 29.0557C82.8389 29.0557 83.415 28.9189 83.9424 28.6455C84.4697 28.3623 84.8604 27.9863 85.1143 27.5176V24.793H83.5322C81.4131 24.793 80.2852 25.5254 80.1484 26.9902L80.1338 27.2393C80.1338 27.7666 80.3193 28.2012 80.6904 28.543C81.0615 28.8848 81.5693 29.0557 82.2139 29.0557ZM96.584 16.1504L96.7158 17.9229C97.8389 16.5459 99.3574 15.8574 101.271 15.8574C103.312 15.8574 104.714 16.6631 105.476 18.2744C106.589 16.6631 108.176 15.8574 110.236 15.8574C111.955 15.8574 113.234 16.3604 114.074 17.3662C114.914 18.3623 115.334 19.8662 115.334 21.8779V32H111.086V21.8926C111.086 20.9941 110.91 20.3398 110.559 19.9297C110.207 19.5098 109.587 19.2998 108.698 19.2998C107.429 19.2998 106.55 19.9053 106.062 21.1162L106.076 32H101.843V21.9072C101.843 20.9893 101.662 20.3252 101.301 19.915C100.939 19.5049 100.324 19.2998 99.4551 19.2998C98.2539 19.2998 97.3848 19.7979 96.8477 20.7939V32H92.6143V16.1504H96.584ZM122.497 16.1504L122.629 17.9814C123.762 16.5654 125.28 15.8574 127.185 15.8574C128.864 15.8574 130.114 16.3506 130.935 17.3369C131.755 18.3232 132.175 19.7979 132.194 21.7607V32H127.961V21.8633C127.961 20.9648 127.766 20.3154 127.375 19.915C126.984 19.5049 126.335 19.2998 125.427 19.2998C124.235 19.2998 123.342 19.8076 122.746 20.8232V32H118.513V16.1504H122.497ZM149.699 24.2217C149.699 26.7607 149.157 28.7432 148.073 30.1689C146.989 31.585 145.476 32.293 143.532 32.293C141.813 32.293 140.441 31.6338 139.416 30.3154L139.226 32H135.417V9.5H139.65V17.5713C140.627 16.4287 141.911 15.8574 143.503 15.8574C145.437 15.8574 146.95 16.5703 148.044 17.9961C149.147 19.4121 149.699 21.4092 149.699 23.9873V24.2217ZM145.466 23.9141C145.466 22.3125 145.212 21.1455 144.704 20.4131C144.196 19.6709 143.439 19.2998 142.434 19.2998C141.086 19.2998 140.158 19.8516 139.65 20.9551V27.21C140.168 28.3232 141.105 28.8799 142.463 28.8799C143.83 28.8799 144.729 28.2061 145.158 26.8584C145.363 26.2139 145.466 25.2324 145.466 23.9141ZM151.662 23.9287C151.662 22.3564 151.965 20.9551 152.57 19.7246C153.176 18.4941 154.045 17.542 155.178 16.8682C156.32 16.1943 157.644 15.8574 159.147 15.8574C161.286 15.8574 163.029 16.5117 164.377 17.8203C165.734 19.1289 166.491 20.9062 166.647 23.1523L166.677 24.2363C166.677 26.668 165.998 28.6211 164.641 30.0957C163.283 31.5605 161.462 32.293 159.177 32.293C156.892 32.293 155.065 31.5605 153.698 30.0957C152.341 28.6309 151.662 26.6387 151.662 24.1191V23.9287ZM155.896 24.2363C155.896 25.7402 156.179 26.8926 156.745 27.6934C157.312 28.4844 158.122 28.8799 159.177 28.8799C160.202 28.8799 161.003 28.4893 161.579 27.708C162.155 26.917 162.443 25.6572 162.443 23.9287C162.443 22.4541 162.155 21.3115 161.579 20.501C161.003 19.6904 160.192 19.2852 159.147 19.2852C158.112 19.2852 157.312 19.6904 156.745 20.501C156.179 21.3018 155.896 22.5469 155.896 24.2363ZM168.625 23.9287C168.625 22.3564 168.928 20.9551 169.533 19.7246C170.139 18.4941 171.008 17.542 172.141 16.8682C173.283 16.1943 174.606 15.8574 176.11 15.8574C178.249 15.8574 179.992 16.5117 181.34 17.8203C182.697 19.1289 183.454 20.9062 183.61 23.1523L183.64 24.2363C183.64 26.668 182.961 28.6211 181.604 30.0957C180.246 31.5605 178.425 32.293 176.14 32.293C173.854 32.293 172.028 31.5605 170.661 30.0957C169.304 28.6309 168.625 26.6387 168.625 24.1191V23.9287ZM172.858 24.2363C172.858 25.7402 173.142 26.8926 173.708 27.6934C174.274 28.4844 175.085 28.8799 176.14 28.8799C177.165 28.8799 177.966 28.4893 178.542 27.708C179.118 26.917 179.406 25.6572 179.406 23.9287C179.406 22.4541 179.118 21.3115 178.542 20.501C177.966 19.6904 177.155 19.2852 176.11 19.2852C175.075 19.2852 174.274 19.6904 173.708 20.501C173.142 21.3018 172.858 22.5469 172.858 24.2363ZM192.004 25.6426L190.48 27.166V32H186.247V9.5H190.48V21.9658L191.301 20.9111L195.358 16.1504H200.441L194.714 22.7568L200.939 32H196.076L192.004 25.6426Z");
    			attr_dev(path1, "fill", "#ED472F");
    			add_location(path1, file$a, 12, 4, 1491);
    			attr_dev(stop0, "stop-color", "#FC6738");
    			add_location(stop0, file$a, 25, 12, 6971);
    			attr_dev(stop1, "offset", "1");
    			attr_dev(stop1, "stop-color", "#DE2727");
    			add_location(stop1, file$a, 26, 12, 7013);
    			attr_dev(linearGradient, "id", "paint0_linear_117:836");
    			attr_dev(linearGradient, "x1", "20.5");
    			attr_dev(linearGradient, "y1", "0");
    			attr_dev(linearGradient, "x2", "20.5");
    			attr_dev(linearGradient, "y2", "41");
    			attr_dev(linearGradient, "gradientUnits", "userSpaceOnUse");
    			add_location(linearGradient, file$a, 17, 8, 6768);
    			add_location(defs, file$a, 16, 4, 6753);
    			attr_dev(svg, "width", "201");
    			attr_dev(svg, "height", "41");
    			attr_dev(svg, "viewBox", "0 0 201 41");
    			attr_dev(svg, "fill", "none");
    			attr_dev(svg, "xmlns", "http://www.w3.org/2000/svg");
    			add_location(svg, file$a, 0, 0, 0);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, svg, anchor);
    			append_dev(svg, rect);
    			append_dev(svg, path0);
    			append_dev(svg, path1);
    			append_dev(svg, defs);
    			append_dev(defs, linearGradient);
    			append_dev(linearGradient, stop0);
    			append_dev(linearGradient, stop1);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(svg);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$a.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$a($$self, $$props) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Logo', slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Logo> was created with unknown prop '${key}'`);
    	});

    	return [];
    }

    class Logo extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$a, create_fragment$a, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Logo",
    			options,
    			id: create_fragment$a.name
    		});
    	}
    }

    /* src/components/DamnbookFriends/Header.svelte generated by Svelte v3.42.4 */
    const file$9 = "src/components/DamnbookFriends/Header.svelte";

    function create_fragment$9(ctx) {
    	let nav;
    	let div13;
    	let logo;
    	let t0;
    	let div5;
    	let div0;
    	let img0;
    	let img0_src_value;
    	let t1;
    	let div1;
    	let img1;
    	let img1_src_value;
    	let t2;
    	let div2;
    	let img2;
    	let img2_src_value;
    	let t3;
    	let div3;
    	let img3;
    	let img3_src_value;
    	let t4;
    	let div4;
    	let img4;
    	let img4_src_value;
    	let t5;
    	let div12;
    	let div6;
    	let img5;
    	let img5_src_value;
    	let t6;
    	let div7;
    	let t8;
    	let div8;
    	let img6;
    	let img6_src_value;
    	let t9;
    	let div9;
    	let img7;
    	let img7_src_value;
    	let t10;
    	let div10;
    	let img8;
    	let img8_src_value;
    	let t11;
    	let div11;
    	let img9;
    	let img9_src_value;
    	let current;
    	logo = new Logo({ $$inline: true });

    	const block = {
    		c: function create() {
    			nav = element("nav");
    			div13 = element("div");
    			create_component(logo.$$.fragment);
    			t0 = space();
    			div5 = element("div");
    			div0 = element("div");
    			img0 = element("img");
    			t1 = space();
    			div1 = element("div");
    			img1 = element("img");
    			t2 = space();
    			div2 = element("div");
    			img2 = element("img");
    			t3 = space();
    			div3 = element("div");
    			img3 = element("img");
    			t4 = space();
    			div4 = element("div");
    			img4 = element("img");
    			t5 = space();
    			div12 = element("div");
    			div6 = element("div");
    			img5 = element("img");
    			t6 = space();
    			div7 = element("div");
    			div7.textContent = "Чувак";
    			t8 = space();
    			div8 = element("div");
    			img6 = element("img");
    			t9 = space();
    			div9 = element("div");
    			img7 = element("img");
    			t10 = space();
    			div10 = element("div");
    			img8 = element("img");
    			t11 = space();
    			div11 = element("div");
    			img9 = element("img");
    			if (!src_url_equal(img0.src, img0_src_value = "./images/Damnbook/Icons/home.png")) attr_dev(img0, "src", img0_src_value);
    			attr_dev(img0, "alt", "Home");
    			attr_dev(img0, "class", "svelte-erbpav");
    			add_location(img0, file$9, 9, 16, 191);
    			attr_dev(div0, "class", "icon svelte-erbpav");
    			add_location(div0, file$9, 8, 12, 156);
    			if (!src_url_equal(img1.src, img1_src_value = "./images/Damnbook/Icons/friends.png")) attr_dev(img1, "src", img1_src_value);
    			attr_dev(img1, "alt", "Home");
    			attr_dev(img1, "class", "svelte-erbpav");
    			add_location(img1, file$9, 12, 16, 325);
    			attr_dev(div1, "class", "icon underline svelte-erbpav");
    			add_location(div1, file$9, 11, 12, 280);
    			if (!src_url_equal(img2.src, img2_src_value = "./images/Damnbook/Icons/video.png")) attr_dev(img2, "src", img2_src_value);
    			attr_dev(img2, "alt", "Home");
    			set_style(img2, "max-height", "70%");
    			set_style(img2, "position", "relative");
    			set_style(img2, "top", "-6px");
    			attr_dev(img2, "class", "svelte-erbpav");
    			add_location(img2, file$9, 15, 16, 479);
    			attr_dev(div2, "class", "icon svelte-erbpav");
    			set_style(div2, "position", "relative");
    			add_location(div2, file$9, 14, 12, 417);
    			if (!src_url_equal(img3.src, img3_src_value = "./images/Damnbook/Icons/shop.png")) attr_dev(img3, "src", img3_src_value);
    			attr_dev(img3, "alt", "Home");
    			attr_dev(img3, "class", "svelte-erbpav");
    			add_location(img3, file$9, 18, 16, 660);
    			attr_dev(div3, "class", "icon svelte-erbpav");
    			add_location(div3, file$9, 17, 12, 625);
    			if (!src_url_equal(img4.src, img4_src_value = "./images/Damnbook/Icons/gang.png")) attr_dev(img4, "src", img4_src_value);
    			attr_dev(img4, "alt", "Home");
    			attr_dev(img4, "class", "svelte-erbpav");
    			add_location(img4, file$9, 21, 16, 784);
    			attr_dev(div4, "class", "icon svelte-erbpav");
    			add_location(div4, file$9, 20, 12, 749);
    			attr_dev(div5, "class", "centralIcons svelte-erbpav");
    			add_location(div5, file$9, 7, 8, 117);
    			attr_dev(img5, "alt", "avatar");
    			if (!src_url_equal(img5.src, img5_src_value = `./images/avatar.png`)) attr_dev(img5, "src", img5_src_value);
    			attr_dev(img5, "class", "svelte-erbpav");
    			add_location(img5, file$9, 27, 16, 959);
    			attr_dev(div6, "class", "avatar svelte-erbpav");
    			add_location(div6, file$9, 26, 12, 922);
    			attr_dev(div7, "class", "username svelte-erbpav");
    			add_location(div7, file$9, 32, 12, 1095);
    			attr_dev(img6, "alt", "avatar");
    			if (!src_url_equal(img6.src, img6_src_value = `./images/Damnbook/Icons/dots.png`)) attr_dev(img6, "src", img6_src_value);
    			attr_dev(img6, "class", "svelte-erbpav");
    			add_location(img6, file$9, 35, 16, 1179);
    			attr_dev(div8, "class", "button svelte-erbpav");
    			add_location(div8, file$9, 34, 12, 1142);
    			attr_dev(img7, "alt", "avatar");
    			if (!src_url_equal(img7.src, img7_src_value = `./images/Damnbook/Icons/messenger.png`)) attr_dev(img7, "src", img7_src_value);
    			attr_dev(img7, "class", "svelte-erbpav");
    			add_location(img7, file$9, 41, 16, 1365);
    			attr_dev(div9, "class", "button svelte-erbpav");
    			add_location(div9, file$9, 40, 12, 1328);
    			attr_dev(img8, "alt", "avatar");
    			if (!src_url_equal(img8.src, img8_src_value = `./images/Damnbook/Icons/bell.png`)) attr_dev(img8, "src", img8_src_value);
    			attr_dev(img8, "class", "svelte-erbpav");
    			add_location(img8, file$9, 47, 16, 1556);
    			attr_dev(div10, "class", "button svelte-erbpav");
    			add_location(div10, file$9, 46, 12, 1519);
    			attr_dev(img9, "alt", "avatar");
    			if (!src_url_equal(img9.src, img9_src_value = `./images/Damnbook/Icons/triangle.png`)) attr_dev(img9, "src", img9_src_value);
    			attr_dev(img9, "class", "svelte-erbpav");
    			add_location(img9, file$9, 53, 16, 1742);
    			attr_dev(div11, "class", "button svelte-erbpav");
    			add_location(div11, file$9, 52, 12, 1705);
    			attr_dev(div12, "class", "rightIcons svelte-erbpav");
    			add_location(div12, file$9, 25, 8, 885);
    			attr_dev(div13, "class", "container svelte-erbpav");
    			add_location(div13, file$9, 5, 4, 68);
    			attr_dev(nav, "class", "svelte-erbpav");
    			add_location(nav, file$9, 4, 0, 58);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, nav, anchor);
    			append_dev(nav, div13);
    			mount_component(logo, div13, null);
    			append_dev(div13, t0);
    			append_dev(div13, div5);
    			append_dev(div5, div0);
    			append_dev(div0, img0);
    			append_dev(div5, t1);
    			append_dev(div5, div1);
    			append_dev(div1, img1);
    			append_dev(div5, t2);
    			append_dev(div5, div2);
    			append_dev(div2, img2);
    			append_dev(div5, t3);
    			append_dev(div5, div3);
    			append_dev(div3, img3);
    			append_dev(div5, t4);
    			append_dev(div5, div4);
    			append_dev(div4, img4);
    			append_dev(div13, t5);
    			append_dev(div13, div12);
    			append_dev(div12, div6);
    			append_dev(div6, img5);
    			append_dev(div12, t6);
    			append_dev(div12, div7);
    			append_dev(div12, t8);
    			append_dev(div12, div8);
    			append_dev(div8, img6);
    			append_dev(div12, t9);
    			append_dev(div12, div9);
    			append_dev(div9, img7);
    			append_dev(div12, t10);
    			append_dev(div12, div10);
    			append_dev(div10, img8);
    			append_dev(div12, t11);
    			append_dev(div12, div11);
    			append_dev(div11, img9);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(logo.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(logo.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(nav);
    			destroy_component(logo);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$9.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$9($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Header', slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Header> was created with unknown prop '${key}'`);
    	});

    	$$self.$capture_state = () => ({ Logo });
    	return [];
    }

    class Header extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$9, create_fragment$9, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Header",
    			options,
    			id: create_fragment$9.name
    		});
    	}
    }

    /* src/components/DamnbookFriends/LeftPanel.svelte generated by Svelte v3.42.4 */

    const file$8 = "src/components/DamnbookFriends/LeftPanel.svelte";

    function get_each_context$3(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[1] = list[i].text;
    	child_ctx[2] = list[i].icon;
    	child_ctx[3] = list[i].isSelected;
    	child_ctx[4] = list[i].hasChevrone;
    	return child_ctx;
    }

    // (61:8) {:else}
    function create_else_block$1(ctx) {
    	let div;

    	const block = {
    		c: function create() {
    			div = element("div");
    			add_location(div, file$8, 61, 12, 1595);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block$1.name,
    		type: "else",
    		source: "(61:8) {:else}",
    		ctx
    	});

    	return block;
    }

    // (57:8) {#if hasChevrone}
    function create_if_block$4(ctx) {
    	let div;
    	let img;
    	let img_src_value;

    	const block = {
    		c: function create() {
    			div = element("div");
    			img = element("img");
    			if (!src_url_equal(img.src, img_src_value = "./images/Damnbook/Icons/Chevrone.png")) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "alt", "icon");
    			attr_dev(img, "class", "svelte-12xxasw");
    			add_location(img, file$8, 58, 16, 1487);
    			attr_dev(div, "class", "icon svelte-12xxasw");
    			add_location(div, file$8, 57, 12, 1452);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, img);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$4.name,
    		type: "if",
    		source: "(57:8) {#if hasChevrone}",
    		ctx
    	});

    	return block;
    }

    // (51:4) {#each menuItems as {text, icon, isSelected, hasChevrone}
    function create_each_block$3(ctx) {
    	let div2;
    	let div1;
    	let div0;
    	let img;
    	let img_src_value;
    	let t0;
    	let t1_value = /*text*/ ctx[1] + "";
    	let t1;
    	let t2;
    	let t3;

    	function select_block_type(ctx, dirty) {
    		if (/*hasChevrone*/ ctx[4]) return create_if_block$4;
    		return create_else_block$1;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block = current_block_type(ctx);

    	const block = {
    		c: function create() {
    			div2 = element("div");
    			div1 = element("div");
    			div0 = element("div");
    			img = element("img");
    			t0 = space();
    			t1 = text(t1_value);
    			t2 = space();
    			if_block.c();
    			t3 = space();
    			if (!src_url_equal(img.src, img_src_value = "./images/Damnbook/Icons/" + /*icon*/ ctx[2])) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "alt", "icon");
    			attr_dev(img, "class", "svelte-12xxasw");
    			add_location(img, file$8, 53, 30, 1319);
    			attr_dev(div0, "class", "icon svelte-12xxasw");
    			add_location(div0, file$8, 53, 12, 1301);
    			attr_dev(div1, "class", "item svelte-12xxasw");
    			add_location(div1, file$8, 52, 8, 1270);
    			attr_dev(div2, "class", "row svelte-12xxasw");
    			toggle_class(div2, "selected", /*isSelected*/ ctx[3]);
    			add_location(div2, file$8, 51, 4, 1216);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div2, anchor);
    			append_dev(div2, div1);
    			append_dev(div1, div0);
    			append_dev(div0, img);
    			append_dev(div1, t0);
    			append_dev(div1, t1);
    			append_dev(div2, t2);
    			if_block.m(div2, null);
    			append_dev(div2, t3);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div2);
    			if_block.d();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$3.name,
    		type: "each",
    		source: "(51:4) {#each menuItems as {text, icon, isSelected, hasChevrone}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$8(ctx) {
    	let div2;
    	let div1;
    	let h1;
    	let t1;
    	let div0;
    	let img;
    	let img_src_value;
    	let t2;
    	let each_value = /*menuItems*/ ctx[0];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$3(get_each_context$3(ctx, each_value, i));
    	}

    	const block = {
    		c: function create() {
    			div2 = element("div");
    			div1 = element("div");
    			h1 = element("h1");
    			h1.textContent = "Знакомые";
    			t1 = space();
    			div0 = element("div");
    			img = element("img");
    			t2 = space();

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			add_location(h1, file$8, 44, 8, 1007);
    			if (!src_url_equal(img.src, img_src_value = "./images/Damnbook/Icons/Gear.png")) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "alt", "icon");
    			attr_dev(img, "class", "svelte-12xxasw");
    			add_location(img, file$8, 46, 12, 1064);
    			attr_dev(div0, "class", "icon svelte-12xxasw");
    			add_location(div0, file$8, 45, 8, 1033);
    			attr_dev(div1, "class", "header svelte-12xxasw");
    			add_location(div1, file$8, 43, 4, 978);
    			attr_dev(div2, "class", "links svelte-12xxasw");
    			add_location(div2, file$8, 42, 0, 954);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div2, anchor);
    			append_dev(div2, div1);
    			append_dev(div1, h1);
    			append_dev(div1, t1);
    			append_dev(div1, div0);
    			append_dev(div0, img);
    			append_dev(div2, t2);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div2, null);
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*menuItems*/ 1) {
    				each_value = /*menuItems*/ ctx[0];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$3(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$3(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div2, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div2);
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$8.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$8($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('LeftPanel', slots, []);

    	const menuItems = [
    		{
    			text: 'Главная',
    			icon: 'Main.png',
    			isSelected: false,
    			hasChevrone: false
    		},
    		{
    			text: 'Запросы на добавление в друзья',
    			icon: 'Arrow.png',
    			isSelected: true,
    			hasChevrone: true
    		},
    		{
    			text: 'Рекомендации',
    			icon: 'Plus.png',
    			isSelected: false,
    			hasChevrone: true
    		},
    		{
    			text: 'Все знакомые',
    			icon: 'List.png',
    			isSelected: false,
    			hasChevrone: true
    		},
    		{
    			text: 'Дни рождения',
    			icon: 'Gift.png',
    			isSelected: false,
    			hasChevrone: false
    		},
    		{
    			text: 'Собственные списки',
    			icon: 'List.png',
    			isSelected: false,
    			hasChevrone: true
    		}
    	];

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<LeftPanel> was created with unknown prop '${key}'`);
    	});

    	$$self.$capture_state = () => ({ menuItems });
    	return [menuItems];
    }

    class LeftPanel extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$8, create_fragment$8, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "LeftPanel",
    			options,
    			id: create_fragment$8.name
    		});
    	}
    }

    /* src/components/DamnbookFriends/FriendBlock.svelte generated by Svelte v3.42.4 */

    const file$7 = "src/components/DamnbookFriends/FriendBlock.svelte";

    // (22:12) {#if item.friendCount}
    function create_if_block$3(ctx) {
    	let img;
    	let img_src_value;
    	let t0;
    	let span;

    	const block = {
    		c: function create() {
    			img = element("img");
    			t0 = space();
    			span = element("span");
    			span.textContent = `${/*friendCountString*/ ctx[2]()}`;
    			attr_dev(img, "class", "commonFace svelte-1jzf5cy");
    			attr_dev(img, "alt", "commonFace");
    			if (!src_url_equal(img.src, img_src_value = `./images/Damnbook/commonFriend.png`)) attr_dev(img, "src", img_src_value);
    			add_location(img, file$7, 22, 12, 720);
    			add_location(span, file$7, 27, 12, 879);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, img, anchor);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, span, anchor);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(img);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(span);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$3.name,
    		type: "if",
    		source: "(22:12) {#if item.friendCount}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$7(ctx) {
    	let div3;
    	let img;
    	let img_src_value;
    	let t0;
    	let div2;
    	let div0;
    	let t1_value = /*item*/ ctx[0].name + "";
    	let t1;
    	let t2;
    	let div1;
    	let t3;
    	let button0;
    	let t5;
    	let button1;
    	let mounted;
    	let dispose;
    	let if_block = /*item*/ ctx[0].friendCount && create_if_block$3(ctx);

    	const block = {
    		c: function create() {
    			div3 = element("div");
    			img = element("img");
    			t0 = space();
    			div2 = element("div");
    			div0 = element("div");
    			t1 = text(t1_value);
    			t2 = space();
    			div1 = element("div");
    			if (if_block) if_block.c();
    			t3 = space();
    			button0 = element("button");
    			button0.textContent = "Добавить";
    			t5 = space();
    			button1 = element("button");
    			button1.textContent = "Удалить";
    			attr_dev(img, "class", "avatar svelte-1jzf5cy");
    			attr_dev(img, "alt", "avatar");
    			if (!src_url_equal(img.src, img_src_value = `./images/Damnbook/Friends/${/*item*/ ctx[0].avatar}`)) attr_dev(img, "src", img_src_value);
    			add_location(img, file$7, 13, 4, 465);
    			attr_dev(div0, "class", "name svelte-1jzf5cy");
    			add_location(div0, file$7, 19, 8, 609);
    			attr_dev(div1, "class", "count svelte-1jzf5cy");
    			add_location(div1, file$7, 20, 8, 653);
    			attr_dev(button0, "class", "add svelte-1jzf5cy");
    			add_location(button0, file$7, 30, 8, 955);
    			attr_dev(button1, "class", "del svelte-1jzf5cy");
    			add_location(button1, file$7, 31, 8, 1055);
    			attr_dev(div2, "class", "info svelte-1jzf5cy");
    			add_location(div2, file$7, 18, 4, 582);
    			attr_dev(div3, "class", "FriendCard svelte-1jzf5cy");
    			add_location(div3, file$7, 12, 0, 436);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div3, anchor);
    			append_dev(div3, img);
    			append_dev(div3, t0);
    			append_dev(div3, div2);
    			append_dev(div2, div0);
    			append_dev(div0, t1);
    			append_dev(div2, t2);
    			append_dev(div2, div1);
    			if (if_block) if_block.m(div1, null);
    			append_dev(div2, t3);
    			append_dev(div2, button0);
    			append_dev(div2, t5);
    			append_dev(div2, button1);

    			if (!mounted) {
    				dispose = listen_dev(button0, "click", /*click_handler*/ ctx[3], false, false, false);
    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*item*/ 1 && !src_url_equal(img.src, img_src_value = `./images/Damnbook/Friends/${/*item*/ ctx[0].avatar}`)) {
    				attr_dev(img, "src", img_src_value);
    			}

    			if (dirty & /*item*/ 1 && t1_value !== (t1_value = /*item*/ ctx[0].name + "")) set_data_dev(t1, t1_value);

    			if (/*item*/ ctx[0].friendCount) {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block$3(ctx);
    					if_block.c();
    					if_block.m(div1, null);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div3);
    			if (if_block) if_block.d();
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$7.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$7($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('FriendBlock', slots, []);
    	let { item } = $$props;

    	let { startAdditionalPlayer = () => {
    		
    	} } = $$props;

    	const friendCountString = () => {
    		const { friendCount } = item;
    		const titles = ['общий друг', 'общих друга', 'общих друзей'];
    		const cases = [2, 0, 1, 1, 1, 2];

    		return friendCount + ' ' + titles[friendCount % 100 > 4 && friendCount % 100 < 20
    		? 2
    		: cases[friendCount % 10 < 5 ? friendCount % 10 : 5]];
    	};

    	const writable_props = ['item', 'startAdditionalPlayer'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<FriendBlock> was created with unknown prop '${key}'`);
    	});

    	const click_handler = () => startAdditionalPlayer(item.videoUrl);

    	$$self.$$set = $$props => {
    		if ('item' in $$props) $$invalidate(0, item = $$props.item);
    		if ('startAdditionalPlayer' in $$props) $$invalidate(1, startAdditionalPlayer = $$props.startAdditionalPlayer);
    	};

    	$$self.$capture_state = () => ({
    		item,
    		startAdditionalPlayer,
    		friendCountString
    	});

    	$$self.$inject_state = $$props => {
    		if ('item' in $$props) $$invalidate(0, item = $$props.item);
    		if ('startAdditionalPlayer' in $$props) $$invalidate(1, startAdditionalPlayer = $$props.startAdditionalPlayer);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [item, startAdditionalPlayer, friendCountString, click_handler];
    }

    class FriendBlock extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$7, create_fragment$7, safe_not_equal, { item: 0, startAdditionalPlayer: 1 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "FriendBlock",
    			options,
    			id: create_fragment$7.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*item*/ ctx[0] === undefined && !('item' in props)) {
    			console.warn("<FriendBlock> was created without expected prop 'item'");
    		}
    	}

    	get item() {
    		throw new Error("<FriendBlock>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set item(value) {
    		throw new Error("<FriendBlock>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get startAdditionalPlayer() {
    		throw new Error("<FriendBlock>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set startAdditionalPlayer(value) {
    		throw new Error("<FriendBlock>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    var friendsList = [
        {
            name: "Александр Дуб",
            avatar: '01.png',
            friendCount: 1,
            videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/7583e301-eea3-440c-9574-5a21475622b7/Sephora 001 010.ism/manifest(format=m3u8-aapl,audio-only=false)',
        },
        {
            name: "Макс Иванов",
            avatar: '02.png',
            friendCount: 1,
        },
        {
            name: "Ира Смолова",
            avatar: '03.png',
            friendCount: 3,
        },
        {
            name: "Игорь Самойлов",
            avatar: '04.png',
            friendCount: 1,
        },
        {
            name: "Jane Moreynis",
            avatar: '05.png',
            friendCount: 1,
        },
        {
            name: "Яна Костылева",
            avatar: '06.png',
            friendCount: 1,
        },

        {
            name: "Роман Ненашев",
            avatar: '07.png',
            friendCount: 1,
        },
        {
            name: "Аня Китаева",
            avatar: '08.png',
            friendCount: 1,
        },
        {
            name: "Айаз Кая",
            avatar: '09.png',
            friendCount: 13,
        },
        {
            name: "Челси Мэннинг",
            avatar: '10.png',
            friendCount: 2,
        },
        {
            name: "Иван Колесов",
            avatar: '11.png',
            friendCount: 1,
        },
        {
            name: "Klaus Jones",
            avatar: '12.png',
            friendCount: 1,
        },

        {
            name: "Джулиан Уайт",
            avatar: '13.png',
            friendCount: 1,
        },
        {
            name: "Рита Симоненко",
            avatar: '14.png',
            friendCount: 0,
        },
        {
            name: "Егор Горин",
            avatar: '15.png',
            friendCount: 1,
        },
        {
            name: "Артур Николаев",
            avatar: '16.png',
            friendCount: 1,
        },
        {
            name: "Армен Абрамян",
            avatar: '17.png',
            friendCount: 1,
        },
        {
            name: "Dude Dudenstein",
            avatar: '18.png',
            friendCount: 1,
        },

        {
            name: "Виктория Ефремова",
            avatar: '19.png',
            friendCount: 1,
        },
        {
            name: "Tema Razumovsky",
            avatar: '20.png',
            friendCount: 1,
        },
        {
            name: "Георгий Шотов",
            avatar: '21.png',
            friendCount: 1,
        },
        {
            name: "Виталий Герасимов",
            avatar: '22.png',
            friendCount: 0,
        },
        {
            name: "Полина Ершова",
            avatar: '23.png',
            friendCount: 1,
        },
        {
            name: "Светлана Хлюпина",
            avatar: '24.png',
            friendCount: 1,
            videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/7583e301-eea3-440c-9574-5a21475622b7/Sephora 001 010.ism/manifest(format=m3u8-aapl,audio-only=false)',
        },
    ];

    /* src/components/DamnbookFriends/FriendsGrid.svelte generated by Svelte v3.42.4 */
    const file$6 = "src/components/DamnbookFriends/FriendsGrid.svelte";

    function get_each_context$2(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[1] = list[i];
    	return child_ctx;
    }

    // (10:8) {#each friendsList as item}
    function create_each_block$2(ctx) {
    	let friendblock;
    	let current;

    	friendblock = new FriendBlock({
    			props: {
    				item: /*item*/ ctx[1],
    				startAdditionalPlayer: /*startAdditionalPlayer*/ ctx[0]
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(friendblock.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(friendblock, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const friendblock_changes = {};
    			if (dirty & /*startAdditionalPlayer*/ 1) friendblock_changes.startAdditionalPlayer = /*startAdditionalPlayer*/ ctx[0];
    			friendblock.$set(friendblock_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(friendblock.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(friendblock.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(friendblock, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$2.name,
    		type: "each",
    		source: "(10:8) {#each friendsList as item}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$6(ctx) {
    	let div1;
    	let h2;
    	let t1;
    	let div0;
    	let current;
    	let each_value = friendsList;
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$2(get_each_context$2(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	const block = {
    		c: function create() {
    			div1 = element("div");
    			h2 = element("h2");
    			h2.textContent = "Кто из них обидчик?";
    			t1 = space();
    			div0 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(h2, "class", "svelte-l1vx3b");
    			add_location(h2, file$6, 7, 4, 197);
    			attr_dev(div0, "class", "gridArray svelte-l1vx3b");
    			add_location(div0, file$6, 8, 4, 230);
    			attr_dev(div1, "class", "gridWrapper svelte-l1vx3b");
    			add_location(div1, file$6, 6, 0, 167);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div1, anchor);
    			append_dev(div1, h2);
    			append_dev(div1, t1);
    			append_dev(div1, div0);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div0, null);
    			}

    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*friendsList, startAdditionalPlayer*/ 1) {
    				each_value = friendsList;
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$2(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block$2(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(div0, null);
    					}
    				}

    				group_outros();

    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div1);
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$6.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$6($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('FriendsGrid', slots, []);

    	let { startAdditionalPlayer = () => {
    		
    	} } = $$props;

    	const writable_props = ['startAdditionalPlayer'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<FriendsGrid> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('startAdditionalPlayer' in $$props) $$invalidate(0, startAdditionalPlayer = $$props.startAdditionalPlayer);
    	};

    	$$self.$capture_state = () => ({
    		FriendBlock,
    		friendsList,
    		startAdditionalPlayer
    	});

    	$$self.$inject_state = $$props => {
    		if ('startAdditionalPlayer' in $$props) $$invalidate(0, startAdditionalPlayer = $$props.startAdditionalPlayer);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [startAdditionalPlayer];
    }

    class FriendsGrid extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$6, create_fragment$6, safe_not_equal, { startAdditionalPlayer: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "FriendsGrid",
    			options,
    			id: create_fragment$6.name
    		});
    	}

    	get startAdditionalPlayer() {
    		throw new Error("<FriendsGrid>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set startAdditionalPlayer(value) {
    		throw new Error("<FriendsGrid>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/DamnbookFriends/index.svelte generated by Svelte v3.42.4 */
    const file$5 = "src/components/DamnbookFriends/index.svelte";

    function create_fragment$5(ctx) {
    	let div2;
    	let header;
    	let t0;
    	let div1;
    	let leftpanel;
    	let t1;
    	let div0;
    	let friendsgrid;
    	let t2;
    	let div3;
    	let video;
    	let current;
    	header = new Header({ $$inline: true });
    	leftpanel = new LeftPanel({ $$inline: true });

    	friendsgrid = new FriendsGrid({
    			props: {
    				startAdditionalPlayer: /*startAdditionalPlayer*/ ctx[2]
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div2 = element("div");
    			create_component(header.$$.fragment);
    			t0 = space();
    			div1 = element("div");
    			create_component(leftpanel.$$.fragment);
    			t1 = space();
    			div0 = element("div");
    			create_component(friendsgrid.$$.fragment);
    			t2 = space();
    			div3 = element("div");
    			video = element("video");
    			attr_dev(div0, "class", "scrollable svelte-1b4r1qe");
    			add_location(div0, file$5, 56, 8, 1496);
    			attr_dev(div1, "class", "main svelte-1b4r1qe");
    			add_location(div1, file$5, 54, 4, 1447);
    			attr_dev(div2, "class", "DamnbookFriends svelte-1b4r1qe");
    			toggle_class(div2, "hidden", /*isInterfaceHidden*/ ctx[0]);
    			add_location(div2, file$5, 52, 0, 1365);
    			attr_dev(video, "id", "additional_player");
    			attr_dev(video, "class", "azuremediaplayer amp-default-skin");
    			video.playsInline = true;
    			add_location(video, file$5, 63, 4, 1719);
    			attr_dev(div3, "class", "videoContainer svelte-1b4r1qe");
    			toggle_class(div3, "hidden", /*isPlayerHidden*/ ctx[1]);
    			add_location(div3, file$5, 61, 0, 1606);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div2, anchor);
    			mount_component(header, div2, null);
    			append_dev(div2, t0);
    			append_dev(div2, div1);
    			mount_component(leftpanel, div1, null);
    			append_dev(div1, t1);
    			append_dev(div1, div0);
    			mount_component(friendsgrid, div0, null);
    			insert_dev(target, t2, anchor);
    			insert_dev(target, div3, anchor);
    			append_dev(div3, video);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*isInterfaceHidden*/ 1) {
    				toggle_class(div2, "hidden", /*isInterfaceHidden*/ ctx[0]);
    			}

    			if (dirty & /*isPlayerHidden*/ 2) {
    				toggle_class(div3, "hidden", /*isPlayerHidden*/ ctx[1]);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(header.$$.fragment, local);
    			transition_in(leftpanel.$$.fragment, local);
    			transition_in(friendsgrid.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(header.$$.fragment, local);
    			transition_out(leftpanel.$$.fragment, local);
    			transition_out(friendsgrid.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div2);
    			destroy_component(header);
    			destroy_component(leftpanel);
    			destroy_component(friendsgrid);
    			if (detaching) detach_dev(t2);
    			if (detaching) detach_dev(div3);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$5.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$5($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('DamnbookFriends', slots, []);
    	let { myPlayer } = $$props;

    	// export let playerWidth;
    	let additionalPlayer;

    	let isInterfaceHidden = false;
    	let isPlayerHidden = true;

    	onMount(() => {
    		myPlayer.pause();

    		additionalPlayer = window.amp(
    			'additional_player',
    			{
    				nativeControlsForTouch: false,
    				autoplay: true,
    				// muted: true,
    				controls: true,
    				fluid: true,
    				height: myPlayer.options.height,
    				width: myPlayer.options.width,
    				logo: { enabled: false }
    			},
    			function () {
    				additionalPlayer.on('ended', () => {
    					$$invalidate(1, isPlayerHidden = true);
    					myPlayer.play();
    				});
    			}
    		);
    	});

    	const startAdditionalPlayer = src => {
    		if (!src) return;
    		additionalPlayer.src([{ src, type: 'application/vnd.ms-sstr+xml' }]);
    		$$invalidate(0, isInterfaceHidden = true);
    		$$invalidate(1, isPlayerHidden = false);
    		additionalPlayer.play();
    	};

    	const writable_props = ['myPlayer'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<DamnbookFriends> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('myPlayer' in $$props) $$invalidate(3, myPlayer = $$props.myPlayer);
    	};

    	$$self.$capture_state = () => ({
    		onMount,
    		Header,
    		LeftPanel,
    		FriendsGrid,
    		myPlayer,
    		additionalPlayer,
    		isInterfaceHidden,
    		isPlayerHidden,
    		startAdditionalPlayer
    	});

    	$$self.$inject_state = $$props => {
    		if ('myPlayer' in $$props) $$invalidate(3, myPlayer = $$props.myPlayer);
    		if ('additionalPlayer' in $$props) additionalPlayer = $$props.additionalPlayer;
    		if ('isInterfaceHidden' in $$props) $$invalidate(0, isInterfaceHidden = $$props.isInterfaceHidden);
    		if ('isPlayerHidden' in $$props) $$invalidate(1, isPlayerHidden = $$props.isPlayerHidden);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [isInterfaceHidden, isPlayerHidden, startAdditionalPlayer, myPlayer];
    }

    class DamnbookFriends extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$5, create_fragment$5, safe_not_equal, { myPlayer: 3 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "DamnbookFriends",
    			options,
    			id: create_fragment$5.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*myPlayer*/ ctx[3] === undefined && !('myPlayer' in props)) {
    			console.warn("<DamnbookFriends> was created without expected prop 'myPlayer'");
    		}
    	}

    	get myPlayer() {
    		throw new Error("<DamnbookFriends>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set myPlayer(value) {
    		throw new Error("<DamnbookFriends>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/Quiz/QuizCard.svelte generated by Svelte v3.42.4 */

    const file$4 = "src/components/Quiz/QuizCard.svelte";

    function get_each_context$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[8] = list[i];
    	child_ctx[10] = i;
    	return child_ctx;
    }

    // (15:4) {#if current.options}
    function create_if_block_2(ctx) {
    	let div;
    	let t0;
    	let t1;

    	const block = {
    		c: function create() {
    			div = element("div");
    			t0 = text(/*state*/ ctx[1]);
    			t1 = text(" / 5");
    			attr_dev(div, "class", "counter svelte-1axyw0x");
    			set_style(div, "top", /*playerWidth*/ ctx[3] * 0.04 + "px");
    			set_style(div, "font-size", /*playerWidth*/ ctx[3] * 0.02 + "px");
    			add_location(div, file$4, 15, 8, 396);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, t0);
    			append_dev(div, t1);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*state*/ 2) set_data_dev(t0, /*state*/ ctx[1]);

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div, "top", /*playerWidth*/ ctx[3] * 0.04 + "px");
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div, "font-size", /*playerWidth*/ ctx[3] * 0.02 + "px");
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_2.name,
    		type: "if",
    		source: "(15:4) {#if current.options}",
    		ctx
    	});

    	return block;
    }

    // (65:12) {:else}
    function create_else_block(ctx) {
    	let div;
    	let t_value = /*current*/ ctx[0].description + "";
    	let t;

    	const block = {
    		c: function create() {
    			div = element("div");
    			t = text(t_value);
    			attr_dev(div, "class", "description svelte-1axyw0x");
    			set_style(div, "font-size", /*playerWidth*/ ctx[3] * 0.018 + "px");
    			add_location(div, file$4, 65, 12, 2898);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, t);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*current*/ 1 && t_value !== (t_value = /*current*/ ctx[0].description + "")) set_data_dev(t, t_value);

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div, "font-size", /*playerWidth*/ ctx[3] * 0.018 + "px");
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block.name,
    		type: "else",
    		source: "(65:12) {:else}",
    		ctx
    	});

    	return block;
    }

    // (28:12) {#if current.options}
    function create_if_block_1$1(ctx) {
    	let div;
    	let each_value = /*current*/ ctx[0].options;
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$1(get_each_context$1(ctx, each_value, i));
    	}

    	const block = {
    		c: function create() {
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(div, "class", "options svelte-1axyw0x");
    			set_style(div, "font-size", /*playerWidth*/ ctx[3] * 0.015 + "px");
    			set_style(div, "border-radius", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			set_style(div, "margin-top", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			add_location(div, file$4, 28, 12, 1088);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*playerWidth, onRadioChange, canContinue, current*/ 57) {
    				each_value = /*current*/ ctx[0].options;
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$1(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$1(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div, "font-size", /*playerWidth*/ ctx[3] * 0.015 + "px");
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div, "border-radius", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div, "margin-top", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1$1.name,
    		type: "if",
    		source: "(28:12) {#if current.options}",
    		ctx
    	});

    	return block;
    }

    // (37:16) {#each current.options as item, index}
    function create_each_block$1(ctx) {
    	let div4;
    	let div2;
    	let div1;
    	let div0;
    	let t0;
    	let t1_value = /*item*/ ctx[8].text + "";
    	let t1;
    	let t2;
    	let div3;

    	let t3_value = (/*canContinue*/ ctx[4] && /*item*/ ctx[8].percent
    	? /*item*/ ctx[8].percent + '%'
    	: '') + "";

    	let t3;
    	let t4;
    	let mounted;
    	let dispose;

    	function click_handler() {
    		return /*click_handler*/ ctx[6](/*index*/ ctx[10]);
    	}

    	const block = {
    		c: function create() {
    			div4 = element("div");
    			div2 = element("div");
    			div1 = element("div");
    			div0 = element("div");
    			t0 = space();
    			t1 = text(t1_value);
    			t2 = space();
    			div3 = element("div");
    			t3 = text(t3_value);
    			t4 = space();
    			attr_dev(div0, "class", "dot svelte-1axyw0x");
    			set_style(div0, "background", /*current*/ ctx[0].button.color);
    			set_style(div0, "width", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			set_style(div0, "height", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			toggle_class(div0, "checked", /*current*/ ctx[0].selected == /*index*/ ctx[10]);
    			add_location(div0, file$4, 48, 32, 2074);
    			attr_dev(div1, "class", "input svelte-1axyw0x");
    			set_style(div1, "width", /*playerWidth*/ ctx[3] * 0.015 + "px");
    			set_style(div1, "height", /*playerWidth*/ ctx[3] * 0.015 + "px");
    			set_style(div1, "border-radius", /*playerWidth*/ ctx[3] * 0.0075 + "px");
    			set_style(div1, "margin-right", /*playerWidth*/ ctx[3] * 0.005 + "px");
    			add_location(div1, file$4, 39, 28, 1598);
    			attr_dev(div2, "class", "label svelte-1axyw0x");
    			add_location(div2, file$4, 38, 24, 1550);
    			attr_dev(div3, "class", "percent svelte-1axyw0x");
    			set_style(div3, "font-size", /*playerWidth*/ ctx[3] * 0.015 + "px");
    			add_location(div3, file$4, 60, 24, 2670);
    			attr_dev(div4, "class", "option svelte-1axyw0x");
    			set_style(div4, "padding", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			add_location(div4, file$4, 37, 20, 1427);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div4, anchor);
    			append_dev(div4, div2);
    			append_dev(div2, div1);
    			append_dev(div1, div0);
    			append_dev(div2, t0);
    			append_dev(div2, t1);
    			append_dev(div4, t2);
    			append_dev(div4, div3);
    			append_dev(div3, t3);
    			append_dev(div4, t4);

    			if (!mounted) {
    				dispose = listen_dev(div4, "click", click_handler, false, false, false);
    				mounted = true;
    			}
    		},
    		p: function update(new_ctx, dirty) {
    			ctx = new_ctx;

    			if (dirty & /*current*/ 1) {
    				set_style(div0, "background", /*current*/ ctx[0].button.color);
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div0, "width", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div0, "height", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			}

    			if (dirty & /*current*/ 1) {
    				toggle_class(div0, "checked", /*current*/ ctx[0].selected == /*index*/ ctx[10]);
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div1, "width", /*playerWidth*/ ctx[3] * 0.015 + "px");
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div1, "height", /*playerWidth*/ ctx[3] * 0.015 + "px");
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div1, "border-radius", /*playerWidth*/ ctx[3] * 0.0075 + "px");
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div1, "margin-right", /*playerWidth*/ ctx[3] * 0.005 + "px");
    			}

    			if (dirty & /*current*/ 1 && t1_value !== (t1_value = /*item*/ ctx[8].text + "")) set_data_dev(t1, t1_value);

    			if (dirty & /*canContinue, current*/ 17 && t3_value !== (t3_value = (/*canContinue*/ ctx[4] && /*item*/ ctx[8].percent
    			? /*item*/ ctx[8].percent + '%'
    			: '') + "")) set_data_dev(t3, t3_value);

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div3, "font-size", /*playerWidth*/ ctx[3] * 0.015 + "px");
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div4, "padding", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div4);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$1.name,
    		type: "each",
    		source: "(37:16) {#each current.options as item, index}",
    		ctx
    	});

    	return block;
    }

    // (69:8) {#if current.button}
    function create_if_block$2(ctx) {
    	let button;
    	let t_value = /*current*/ ctx[0].button.text + "";
    	let t;
    	let button_disabled_value;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			button = element("button");
    			t = text(t_value);
    			set_style(button, "background", /*current*/ ctx[0].button.color);
    			set_style(button, "opacity", /*canContinue*/ ctx[4] ? 1 : 0);
    			set_style(button, "cursor", /*canContinue*/ ctx[4] ? 'pointer' : 'default');
    			set_style(button, "padding", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			set_style(button, "font-size", /*playerWidth*/ ctx[3] * 0.03 + "px");
    			set_style(button, "border-radius", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			set_style(button, "margin-top", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			button.disabled = button_disabled_value = !/*canContinue*/ ctx[4];
    			attr_dev(button, "class", "svelte-1axyw0x");
    			add_location(button, file$4, 69, 12, 3068);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, button, anchor);
    			append_dev(button, t);

    			if (!mounted) {
    				dispose = listen_dev(
    					button,
    					"click",
    					function () {
    						if (is_function(/*incrementState*/ ctx[2])) /*incrementState*/ ctx[2].apply(this, arguments);
    					},
    					false,
    					false,
    					false
    				);

    				mounted = true;
    			}
    		},
    		p: function update(new_ctx, dirty) {
    			ctx = new_ctx;
    			if (dirty & /*current*/ 1 && t_value !== (t_value = /*current*/ ctx[0].button.text + "")) set_data_dev(t, t_value);

    			if (dirty & /*current*/ 1) {
    				set_style(button, "background", /*current*/ ctx[0].button.color);
    			}

    			if (dirty & /*canContinue*/ 16) {
    				set_style(button, "opacity", /*canContinue*/ ctx[4] ? 1 : 0);
    			}

    			if (dirty & /*canContinue*/ 16) {
    				set_style(button, "cursor", /*canContinue*/ ctx[4] ? 'pointer' : 'default');
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(button, "padding", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(button, "font-size", /*playerWidth*/ ctx[3] * 0.03 + "px");
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(button, "border-radius", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(button, "margin-top", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			}

    			if (dirty & /*canContinue*/ 16 && button_disabled_value !== (button_disabled_value = !/*canContinue*/ ctx[4])) {
    				prop_dev(button, "disabled", button_disabled_value);
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(button);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$2.name,
    		type: "if",
    		source: "(69:8) {#if current.button}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$4(ctx) {
    	let div0;
    	let t0;
    	let div4;
    	let t1;
    	let div3;
    	let div2;
    	let div1;
    	let t2_value = /*current*/ ctx[0].title + "";
    	let t2;
    	let t3;
    	let img;
    	let img_src_value;
    	let img_alt_value;
    	let t4;
    	let t5;
    	let mounted;
    	let dispose;
    	let if_block0 = /*current*/ ctx[0].options && create_if_block_2(ctx);

    	function select_block_type(ctx, dirty) {
    		if (/*current*/ ctx[0].options) return create_if_block_1$1;
    		return create_else_block;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block1 = current_block_type(ctx);
    	let if_block2 = /*current*/ ctx[0].button && create_if_block$2(ctx);

    	const block = {
    		c: function create() {
    			div0 = element("div");
    			t0 = space();
    			div4 = element("div");
    			if (if_block0) if_block0.c();
    			t1 = space();
    			div3 = element("div");
    			div2 = element("div");
    			div1 = element("div");
    			t2 = text(t2_value);
    			t3 = space();
    			img = element("img");
    			t4 = space();
    			if_block1.c();
    			t5 = space();
    			if (if_block2) if_block2.c();
    			attr_dev(div0, "class", "background svelte-1axyw0x");
    			add_location(div0, file$4, 12, 0, 307);
    			attr_dev(div1, "class", "title svelte-1axyw0x");
    			set_style(div1, "font-size", /*playerWidth*/ ctx[3] * 0.018 + "px");
    			set_style(div1, "margin-bottom", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			add_location(div1, file$4, 23, 12, 806);
    			if (!src_url_equal(img.src, img_src_value = "./images/quiz/" + /*current*/ ctx[0].image + ".png")) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "alt", img_alt_value = /*current*/ ctx[0].image);
    			attr_dev(img, "class", "svelte-1axyw0x");
    			add_location(img, file$4, 26, 12, 972);
    			attr_dev(div2, "class", "block svelte-1axyw0x");
    			set_style(div2, "padding", /*playerWidth*/ ctx[3] * 0.015 + "px");
    			set_style(div2, "border-radius", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			add_location(div2, file$4, 18, 8, 588);
    			attr_dev(div3, "class", "card svelte-1axyw0x");
    			set_style(div3, "max-width", /*playerWidth*/ ctx[3] * 0.4 + "px");
    			add_location(div3, file$4, 17, 4, 520);
    			attr_dev(div4, "class", "cardContainer svelte-1axyw0x");
    			add_location(div4, file$4, 13, 0, 334);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div0, anchor);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, div4, anchor);
    			if (if_block0) if_block0.m(div4, null);
    			append_dev(div4, t1);
    			append_dev(div4, div3);
    			append_dev(div3, div2);
    			append_dev(div2, div1);
    			append_dev(div1, t2);
    			append_dev(div2, t3);
    			append_dev(div2, img);
    			append_dev(div2, t4);
    			if_block1.m(div2, null);
    			append_dev(div3, t5);
    			if (if_block2) if_block2.m(div3, null);

    			if (!mounted) {
    				dispose = listen_dev(div2, "click", /*click_handler_1*/ ctx[7], false, false, false);
    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (/*current*/ ctx[0].options) {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);
    				} else {
    					if_block0 = create_if_block_2(ctx);
    					if_block0.c();
    					if_block0.m(div4, t1);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (dirty & /*current*/ 1 && t2_value !== (t2_value = /*current*/ ctx[0].title + "")) set_data_dev(t2, t2_value);

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div1, "font-size", /*playerWidth*/ ctx[3] * 0.018 + "px");
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div1, "margin-bottom", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			}

    			if (dirty & /*current*/ 1 && !src_url_equal(img.src, img_src_value = "./images/quiz/" + /*current*/ ctx[0].image + ".png")) {
    				attr_dev(img, "src", img_src_value);
    			}

    			if (dirty & /*current*/ 1 && img_alt_value !== (img_alt_value = /*current*/ ctx[0].image)) {
    				attr_dev(img, "alt", img_alt_value);
    			}

    			if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block1) {
    				if_block1.p(ctx, dirty);
    			} else {
    				if_block1.d(1);
    				if_block1 = current_block_type(ctx);

    				if (if_block1) {
    					if_block1.c();
    					if_block1.m(div2, null);
    				}
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div2, "padding", /*playerWidth*/ ctx[3] * 0.015 + "px");
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div2, "border-radius", /*playerWidth*/ ctx[3] * 0.01 + "px");
    			}

    			if (/*current*/ ctx[0].button) {
    				if (if_block2) {
    					if_block2.p(ctx, dirty);
    				} else {
    					if_block2 = create_if_block$2(ctx);
    					if_block2.c();
    					if_block2.m(div3, null);
    				}
    			} else if (if_block2) {
    				if_block2.d(1);
    				if_block2 = null;
    			}

    			if (dirty & /*playerWidth*/ 8) {
    				set_style(div3, "max-width", /*playerWidth*/ ctx[3] * 0.4 + "px");
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div0);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(div4);
    			if (if_block0) if_block0.d();
    			if_block1.d();
    			if (if_block2) if_block2.d();
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$4.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$4($$self, $$props, $$invalidate) {
    	let canContinue;
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('QuizCard', slots, []);
    	let { state } = $$props;

    	let { incrementState = () => {
    		
    	} } = $$props;

    	let { current } = $$props;
    	let { playerWidth } = $$props;

    	const onRadioChange = index => {
    		if (current.selected === null) $$invalidate(0, current.selected = +index, current);
    	};

    	const writable_props = ['state', 'incrementState', 'current', 'playerWidth'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<QuizCard> was created with unknown prop '${key}'`);
    	});

    	const click_handler = index => onRadioChange(index);

    	const click_handler_1 = () => {
    		if (current.description) incrementState();
    	};

    	$$self.$$set = $$props => {
    		if ('state' in $$props) $$invalidate(1, state = $$props.state);
    		if ('incrementState' in $$props) $$invalidate(2, incrementState = $$props.incrementState);
    		if ('current' in $$props) $$invalidate(0, current = $$props.current);
    		if ('playerWidth' in $$props) $$invalidate(3, playerWidth = $$props.playerWidth);
    	};

    	$$self.$capture_state = () => ({
    		state,
    		incrementState,
    		current,
    		playerWidth,
    		onRadioChange,
    		canContinue
    	});

    	$$self.$inject_state = $$props => {
    		if ('state' in $$props) $$invalidate(1, state = $$props.state);
    		if ('incrementState' in $$props) $$invalidate(2, incrementState = $$props.incrementState);
    		if ('current' in $$props) $$invalidate(0, current = $$props.current);
    		if ('playerWidth' in $$props) $$invalidate(3, playerWidth = $$props.playerWidth);
    		if ('canContinue' in $$props) $$invalidate(4, canContinue = $$props.canContinue);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*current*/ 1) {
    			$$invalidate(4, canContinue = typeof current.selected === 'number');
    		}
    	};

    	return [
    		current,
    		state,
    		incrementState,
    		playerWidth,
    		canContinue,
    		onRadioChange,
    		click_handler,
    		click_handler_1
    	];
    }

    class QuizCard extends SvelteComponentDev {
    	constructor(options) {
    		super(options);

    		init(this, options, instance$4, create_fragment$4, safe_not_equal, {
    			state: 1,
    			incrementState: 2,
    			current: 0,
    			playerWidth: 3
    		});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "QuizCard",
    			options,
    			id: create_fragment$4.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*state*/ ctx[1] === undefined && !('state' in props)) {
    			console.warn("<QuizCard> was created without expected prop 'state'");
    		}

    		if (/*current*/ ctx[0] === undefined && !('current' in props)) {
    			console.warn("<QuizCard> was created without expected prop 'current'");
    		}

    		if (/*playerWidth*/ ctx[3] === undefined && !('playerWidth' in props)) {
    			console.warn("<QuizCard> was created without expected prop 'playerWidth'");
    		}
    	}

    	get state() {
    		throw new Error("<QuizCard>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set state(value) {
    		throw new Error("<QuizCard>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get incrementState() {
    		throw new Error("<QuizCard>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set incrementState(value) {
    		throw new Error("<QuizCard>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get current() {
    		throw new Error("<QuizCard>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set current(value) {
    		throw new Error("<QuizCard>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get playerWidth() {
    		throw new Error("<QuizCard>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set playerWidth(value) {
    		throw new Error("<QuizCard>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/Quiz/index.svelte generated by Svelte v3.42.4 */
    const file$3 = "src/components/Quiz/index.svelte";

    // (107:28) 
    function create_if_block_1(ctx) {
    	let quizcard;
    	let current;

    	quizcard = new QuizCard({
    			props: {
    				state: /*state*/ ctx[1],
    				incrementState: /*incrementState*/ ctx[3],
    				current: /*current*/ ctx[2],
    				playerWidth: /*playerWidth*/ ctx[0]
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(quizcard.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(quizcard, target, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const quizcard_changes = {};
    			if (dirty & /*state*/ 2) quizcard_changes.state = /*state*/ ctx[1];
    			if (dirty & /*current*/ 4) quizcard_changes.current = /*current*/ ctx[2];
    			if (dirty & /*playerWidth*/ 1) quizcard_changes.playerWidth = /*playerWidth*/ ctx[0];
    			quizcard.$set(quizcard_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(quizcard.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(quizcard.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(quizcard, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1.name,
    		type: "if",
    		source: "(107:28) ",
    		ctx
    	});

    	return block;
    }

    // (96:8) {#if state == 0}
    function create_if_block$1(ctx) {
    	let button;
    	let t;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			button = element("button");
    			t = text("ПРОЙТИ");
    			set_style(button, "font-size", /*playerWidth*/ ctx[0] * 0.035 + "px");
    			set_style(button, "border-radius", /*playerWidth*/ ctx[0] * 0.03 + "px");
    			set_style(button, "padding", /*playerWidth*/ ctx[0] * 0.01 + "px " + /*playerWidth*/ ctx[0] * 0.04 + "px");
    			attr_dev(button, "class", "svelte-c2xwt3");
    			add_location(button, file$3, 96, 12, 2815);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, button, anchor);
    			append_dev(button, t);

    			if (!mounted) {
    				dispose = listen_dev(button, "click", /*incrementState*/ ctx[3], false, false, false);
    				mounted = true;
    			}
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*playerWidth*/ 1) {
    				set_style(button, "font-size", /*playerWidth*/ ctx[0] * 0.035 + "px");
    			}

    			if (dirty & /*playerWidth*/ 1) {
    				set_style(button, "border-radius", /*playerWidth*/ ctx[0] * 0.03 + "px");
    			}

    			if (dirty & /*playerWidth*/ 1) {
    				set_style(button, "padding", /*playerWidth*/ ctx[0] * 0.01 + "px " + /*playerWidth*/ ctx[0] * 0.04 + "px");
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(button);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block$1.name,
    		type: "if",
    		source: "(96:8) {#if state == 0}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$3(ctx) {
    	let div1;
    	let div0;
    	let current_block_type_index;
    	let if_block;
    	let current;
    	const if_block_creators = [create_if_block$1, create_if_block_1];
    	const if_blocks = [];

    	function select_block_type(ctx, dirty) {
    		if (/*state*/ ctx[1] == 0) return 0;
    		if (/*state*/ ctx[1] < 7) return 1;
    		return -1;
    	}

    	if (~(current_block_type_index = select_block_type(ctx))) {
    		if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    	}

    	const block = {
    		c: function create() {
    			div1 = element("div");
    			div0 = element("div");
    			if (if_block) if_block.c();
    			attr_dev(div0, "class", "background svelte-c2xwt3");
    			add_location(div0, file$3, 94, 4, 2753);
    			attr_dev(div1, "class", "container svelte-c2xwt3");
    			add_location(div1, file$3, 93, 0, 2725);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div1, anchor);
    			append_dev(div1, div0);

    			if (~current_block_type_index) {
    				if_blocks[current_block_type_index].m(div0, null);
    			}

    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			let previous_block_index = current_block_type_index;
    			current_block_type_index = select_block_type(ctx);

    			if (current_block_type_index === previous_block_index) {
    				if (~current_block_type_index) {
    					if_blocks[current_block_type_index].p(ctx, dirty);
    				}
    			} else {
    				if (if_block) {
    					group_outros();

    					transition_out(if_blocks[previous_block_index], 1, 1, () => {
    						if_blocks[previous_block_index] = null;
    					});

    					check_outros();
    				}

    				if (~current_block_type_index) {
    					if_block = if_blocks[current_block_type_index];

    					if (!if_block) {
    						if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
    						if_block.c();
    					} else {
    						if_block.p(ctx, dirty);
    					}

    					transition_in(if_block, 1);
    					if_block.m(div0, null);
    				} else {
    					if_block = null;
    				}
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div1);

    			if (~current_block_type_index) {
    				if_blocks[current_block_type_index].d();
    			}
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$3.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$3($$self, $$props, $$invalidate) {
    	let current;
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Quiz', slots, []);
    	let { myPlayer } = $$props;
    	let { playerWidth } = $$props;

    	onMount(() => {
    		myPlayer.pause();
    		myPlayer.controls(false);
    	});

    	const continuePlay = () => {
    		myPlayer.controls(true);
    		myPlayer.play();
    	};

    	let state = 0;
    	const incrementState = () => $$invalidate(1, state += 1);

    	const quizArray = [
    		{},
    		{
    			title: 'Часто сидишь и смотришь часами обзоры на товары, в которых нет нужды?',
    			image: 'man_with_phone',
    			options: [
    				{ text: 'Очень (', percent: 83 },
    				{
    					text: 'Кто сказал, что в них нет нужды?!',
    					percent: 17
    				}
    			],
    			button: { text: 'Далее', color: '#88D44C' },
    			hasIndex: true,
    			selected: null
    		},
    		{
    			title: 'Может побалуем себя чем-то?',
    			image: 'thinking_girl',
    			options: [
    				{ text: 'Было бы здорово...', percent: 35 },
    				{
    					text: 'Это не баловство, а действенный метод! Живем один раз.',
    					percent: 65
    				}
    			],
    			button: { text: 'Далее', color: '#88D44C' },
    			hasIndex: true,
    			selected: null
    		},
    		{
    			title: 'У вас много «полезных» подписок?',
    			image: 'man_with_tab',
    			options: [{ text: 'А у вас?', percent: 42 }, { text: 'Все полезные!', percent: 58 }],
    			button: { text: 'Далее', color: '#88D44C' },
    			hasIndex: true,
    			selected: null
    		},
    		{
    			title: 'А тесты нравятся такие?',
    			image: 'pencil',
    			options: [{ text: 'Да', percent: 51 }, { text: 'Конечно', percent: 49 }],
    			button: { text: 'Далее', color: '#88D44C' },
    			hasIndex: true,
    			selected: null
    		},
    		{
    			title: 'Чувствую сарказм... ',
    			image: 'thinking_man',
    			options: [{ text: 'Да покажи уже результат!' }, { text: 'Правильно делаешь…' }],
    			button: {
    				text: 'Показать результат',
    				color: '#4C72D4'
    			},
    			hasIndex: true,
    			selected: null
    		},
    		{
    			title: 'РЕЗУЛЬТАТ',
    			image: 'surprised_man',
    			button: null,
    			description: `
                Возможно вы шопоголик, а может и нет!

                Я простой онлайн тест, у вас завышенные ожидания от меня…
            `
    		}
    	];

    	const writable_props = ['myPlayer', 'playerWidth'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Quiz> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('myPlayer' in $$props) $$invalidate(4, myPlayer = $$props.myPlayer);
    		if ('playerWidth' in $$props) $$invalidate(0, playerWidth = $$props.playerWidth);
    	};

    	$$self.$capture_state = () => ({
    		onMount,
    		QuizCard,
    		myPlayer,
    		playerWidth,
    		continuePlay,
    		state,
    		incrementState,
    		quizArray,
    		current
    	});

    	$$self.$inject_state = $$props => {
    		if ('myPlayer' in $$props) $$invalidate(4, myPlayer = $$props.myPlayer);
    		if ('playerWidth' in $$props) $$invalidate(0, playerWidth = $$props.playerWidth);
    		if ('state' in $$props) $$invalidate(1, state = $$props.state);
    		if ('current' in $$props) $$invalidate(2, current = $$props.current);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*state*/ 2) {
    			if (state > 6) continuePlay();
    		}

    		if ($$self.$$.dirty & /*state*/ 2) {
    			$$invalidate(2, current = quizArray[state]);
    		}
    	};

    	return [playerWidth, state, current, incrementState, myPlayer];
    }

    class Quiz extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$3, create_fragment$3, safe_not_equal, { myPlayer: 4, playerWidth: 0 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Quiz",
    			options,
    			id: create_fragment$3.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*myPlayer*/ ctx[4] === undefined && !('myPlayer' in props)) {
    			console.warn("<Quiz> was created without expected prop 'myPlayer'");
    		}

    		if (/*playerWidth*/ ctx[0] === undefined && !('playerWidth' in props)) {
    			console.warn("<Quiz> was created without expected prop 'playerWidth'");
    		}
    	}

    	get myPlayer() {
    		throw new Error("<Quiz>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set myPlayer(value) {
    		throw new Error("<Quiz>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get playerWidth() {
    		throw new Error("<Quiz>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set playerWidth(value) {
    		throw new Error("<Quiz>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src/components/ActiveElements.svelte generated by Svelte v3.42.4 */

    const { console: console_1 } = globals;
    const file$2 = "src/components/ActiveElements.svelte";

    function get_each_context(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[7] = list[i];
    	return child_ctx;
    }

    // (39:2) {#if activeElements}
    function create_if_block(ctx) {
    	let each_1_anchor;
    	let current;
    	let each_value = /*activeElements*/ ctx[2];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
    	}

    	const out = i => transition_out(each_blocks[i], 1, 1, () => {
    		each_blocks[i] = null;
    	});

    	const block = {
    		c: function create() {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			each_1_anchor = empty();
    		},
    		m: function mount(target, anchor) {
    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(target, anchor);
    			}

    			insert_dev(target, each_1_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*elementTypes, activeElements, myPlayer, playerWidth*/ 23) {
    				each_value = /*activeElements*/ ctx[2];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    						transition_in(each_blocks[i], 1);
    					} else {
    						each_blocks[i] = create_each_block(child_ctx);
    						each_blocks[i].c();
    						transition_in(each_blocks[i], 1);
    						each_blocks[i].m(each_1_anchor.parentNode, each_1_anchor);
    					}
    				}

    				group_outros();

    				for (i = each_value.length; i < each_blocks.length; i += 1) {
    					out(i);
    				}

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;

    			for (let i = 0; i < each_value.length; i += 1) {
    				transition_in(each_blocks[i]);
    			}

    			current = true;
    		},
    		o: function outro(local) {
    			each_blocks = each_blocks.filter(Boolean);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				transition_out(each_blocks[i]);
    			}

    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach_dev(each_1_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block.name,
    		type: "if",
    		source: "(39:2) {#if activeElements}",
    		ctx
    	});

    	return block;
    }

    // (40:4) {#each activeElements as item}
    function create_each_block(ctx) {
    	let switch_instance;
    	let switch_instance_anchor;
    	let current;
    	var switch_value = /*elementTypes*/ ctx[4][/*item*/ ctx[7].type];

    	function switch_props(ctx) {
    		return {
    			props: {
    				myPlayer: /*myPlayer*/ ctx[1],
    				playerWidth: /*playerWidth*/ ctx[0],
    				item: /*item*/ ctx[7]
    			},
    			$$inline: true
    		};
    	}

    	if (switch_value) {
    		switch_instance = new switch_value(switch_props(ctx));
    	}

    	const block = {
    		c: function create() {
    			if (switch_instance) create_component(switch_instance.$$.fragment);
    			switch_instance_anchor = empty();
    		},
    		m: function mount(target, anchor) {
    			if (switch_instance) {
    				mount_component(switch_instance, target, anchor);
    			}

    			insert_dev(target, switch_instance_anchor, anchor);
    			current = true;
    		},
    		p: function update(ctx, dirty) {
    			const switch_instance_changes = {};
    			if (dirty & /*myPlayer*/ 2) switch_instance_changes.myPlayer = /*myPlayer*/ ctx[1];
    			if (dirty & /*playerWidth*/ 1) switch_instance_changes.playerWidth = /*playerWidth*/ ctx[0];
    			if (dirty & /*activeElements*/ 4) switch_instance_changes.item = /*item*/ ctx[7];

    			if (switch_value !== (switch_value = /*elementTypes*/ ctx[4][/*item*/ ctx[7].type])) {
    				if (switch_instance) {
    					group_outros();
    					const old_component = switch_instance;

    					transition_out(old_component.$$.fragment, 1, 0, () => {
    						destroy_component(old_component, 1);
    					});

    					check_outros();
    				}

    				if (switch_value) {
    					switch_instance = new switch_value(switch_props(ctx));
    					create_component(switch_instance.$$.fragment);
    					transition_in(switch_instance.$$.fragment, 1);
    					mount_component(switch_instance, switch_instance_anchor.parentNode, switch_instance_anchor);
    				} else {
    					switch_instance = null;
    				}
    			} else if (switch_value) {
    				switch_instance.$set(switch_instance_changes);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			if (switch_instance) transition_in(switch_instance.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			if (switch_instance) transition_out(switch_instance.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(switch_instance_anchor);
    			if (switch_instance) destroy_component(switch_instance, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block.name,
    		type: "each",
    		source: "(40:4) {#each activeElements as item}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$2(ctx) {
    	let div;
    	let current;
    	let mounted;
    	let dispose;
    	let if_block = /*activeElements*/ ctx[2] && create_if_block(ctx);

    	const block = {
    		c: function create() {
    			div = element("div");
    			if (if_block) if_block.c();
    			attr_dev(div, "class", "overlay svelte-3myjl3");
    			add_location(div, file$2, 30, 0, 1103);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			if (if_block) if_block.m(div, null);
    			current = true;

    			if (!mounted) {
    				dispose = [
    					listen_dev(div, "mousedown", /*forwardEvent*/ ctx[3], false, false, false),
    					listen_dev(div, "mouseup", /*forwardEvent*/ ctx[3], false, false, false),
    					listen_dev(div, "touchstart", /*forwardEvent*/ ctx[3], false, false, false),
    					listen_dev(div, "touchend", /*forwardEvent*/ ctx[3], false, false, false),
    					listen_dev(div, "mousemove", /*forwardEvent*/ ctx[3], false, false, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			if (/*activeElements*/ ctx[2]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);

    					if (dirty & /*activeElements*/ 4) {
    						transition_in(if_block, 1);
    					}
    				} else {
    					if_block = create_if_block(ctx);
    					if_block.c();
    					transition_in(if_block, 1);
    					if_block.m(div, null);
    				}
    			} else if (if_block) {
    				group_outros();

    				transition_out(if_block, 1, 1, () => {
    					if_block = null;
    				});

    				check_outros();
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(if_block);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(if_block);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			if (if_block) if_block.d();
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$2.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$2($$self, $$props, $$invalidate) {
    	let activeElements;
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('ActiveElements', slots, []);
    	let { playerWidth } = $$props;
    	let { currentTime } = $$props;
    	let { myPlayer } = $$props;
    	let { containerRef } = $$props;

    	const forwardEvent = event => {
    		const newEvent = event.touches
    		? new TouchEvent(event.type, event)
    		: new MouseEvent(event.type, event);

    		const player = containerRef?.querySelector('.vjs-player');
    		if (player) player.dispatchEvent(newEvent);
    	};

    	const elementTypes = {
    		Zone,
    		Frame,
    		SpamHell,
    		Foodporn,
    		InstagramScroll,
    		InstagramMask,
    		DamnbookFriends,
    		Quiz
    	};

    	console.log(activeElements);
    	const writable_props = ['playerWidth', 'currentTime', 'myPlayer', 'containerRef'];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console_1.warn(`<ActiveElements> was created with unknown prop '${key}'`);
    	});

    	$$self.$$set = $$props => {
    		if ('playerWidth' in $$props) $$invalidate(0, playerWidth = $$props.playerWidth);
    		if ('currentTime' in $$props) $$invalidate(5, currentTime = $$props.currentTime);
    		if ('myPlayer' in $$props) $$invalidate(1, myPlayer = $$props.myPlayer);
    		if ('containerRef' in $$props) $$invalidate(6, containerRef = $$props.containerRef);
    	};

    	$$self.$capture_state = () => ({
    		sequence,
    		Zone,
    		Frame,
    		SpamHell,
    		Foodporn,
    		InstagramScroll,
    		InstagramMask,
    		DamnbookFriends,
    		Quiz,
    		playerWidth,
    		currentTime,
    		myPlayer,
    		containerRef,
    		forwardEvent,
    		elementTypes,
    		activeElements
    	});

    	$$self.$inject_state = $$props => {
    		if ('playerWidth' in $$props) $$invalidate(0, playerWidth = $$props.playerWidth);
    		if ('currentTime' in $$props) $$invalidate(5, currentTime = $$props.currentTime);
    		if ('myPlayer' in $$props) $$invalidate(1, myPlayer = $$props.myPlayer);
    		if ('containerRef' in $$props) $$invalidate(6, containerRef = $$props.containerRef);
    		if ('activeElements' in $$props) $$invalidate(2, activeElements = $$props.activeElements);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*currentTime*/ 32) {
    			$$invalidate(2, activeElements = sequence.reduce(
    				(memo, item) => {
    					return currentTime >= item.start && currentTime <= item.end
    					? [...memo, item]
    					: memo;
    				},
    				[]
    			));
    		}
    	};

    	return [
    		playerWidth,
    		myPlayer,
    		activeElements,
    		forwardEvent,
    		elementTypes,
    		currentTime,
    		containerRef
    	];
    }

    class ActiveElements extends SvelteComponentDev {
    	constructor(options) {
    		super(options);

    		init(this, options, instance$2, create_fragment$2, safe_not_equal, {
    			playerWidth: 0,
    			currentTime: 5,
    			myPlayer: 1,
    			containerRef: 6
    		});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "ActiveElements",
    			options,
    			id: create_fragment$2.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*playerWidth*/ ctx[0] === undefined && !('playerWidth' in props)) {
    			console_1.warn("<ActiveElements> was created without expected prop 'playerWidth'");
    		}

    		if (/*currentTime*/ ctx[5] === undefined && !('currentTime' in props)) {
    			console_1.warn("<ActiveElements> was created without expected prop 'currentTime'");
    		}

    		if (/*myPlayer*/ ctx[1] === undefined && !('myPlayer' in props)) {
    			console_1.warn("<ActiveElements> was created without expected prop 'myPlayer'");
    		}

    		if (/*containerRef*/ ctx[6] === undefined && !('containerRef' in props)) {
    			console_1.warn("<ActiveElements> was created without expected prop 'containerRef'");
    		}
    	}

    	get playerWidth() {
    		throw new Error("<ActiveElements>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set playerWidth(value) {
    		throw new Error("<ActiveElements>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get currentTime() {
    		throw new Error("<ActiveElements>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set currentTime(value) {
    		throw new Error("<ActiveElements>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get myPlayer() {
    		throw new Error("<ActiveElements>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set myPlayer(value) {
    		throw new Error("<ActiveElements>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get containerRef() {
    		throw new Error("<ActiveElements>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set containerRef(value) {
    		throw new Error("<ActiveElements>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    (() => {
        if (location.href.includes("gitlab"))
          return "https://az67128.gitlab.io/7-sins";
        if (location.href.includes("localhost") || location.href.includes("192.168"))
          return ".";
        return "https://az67128.gitlab.io/7-sins";
      })();


      function isIos() {
        return (
          [
            "iPad Simulator",
            "iPhone Simulator",
            "iPod Simulator",
            "iPad",
            "iPhone",
            "iPod",
          ].includes(navigator.platform) ||
          // iPad on iOS 13 detection
          (navigator.userAgent.includes("Mac") && "ontouchend" in document)
        );
      }

    !function(a){function b(b,c){var d=a.createEvent("Event");d.initEvent(b,!0,!1),c.dispatchEvent(d);}function c(c){c.stopPropagation(),c.stopImmediatePropagation(),a[j.enabled]=a[f.enabled],a[j.element]=a[f.element],b(j.events.change,c.target);}function d(a){b(j.events.error,a.target);}function e(b){return function(c,d){function e(){c(),a.removeEventListener(f.events.change,e,!1);}function g(){d(new TypeError),a.removeEventListener(f.events.error,g,!1);}return b!==j.exit||a[f.element]?(a.addEventListener(f.events.change,e,!1),void a.addEventListener(f.events.error,g,!1)):void setTimeout(function(){d(new TypeError);},1)}}var f,g,i={w3:{enabled:"fullscreenEnabled",element:"fullscreenElement",request:"requestFullscreen",exit:"exitFullscreen",events:{change:"fullscreenchange",error:"fullscreenerror"}},webkit:{enabled:"webkitFullscreenEnabled",element:"webkitCurrentFullScreenElement",request:"webkitRequestFullscreen",exit:"webkitExitFullscreen",events:{change:"webkitfullscreenchange",error:"webkitfullscreenerror"}},moz:{enabled:"mozFullScreenEnabled",element:"mozFullScreenElement",request:"mozRequestFullScreen",exit:"mozCancelFullScreen",events:{change:"mozfullscreenchange",error:"mozfullscreenerror"}},ms:{enabled:"msFullscreenEnabled",element:"msFullscreenElement",request:"msRequestFullscreen",exit:"msExitFullscreen",events:{change:"MSFullscreenChange",error:"MSFullscreenError"}}},j=i.w3;for(g in i)if(i[g].enabled in a){f=i[g];break}return j.enabled in a||!f||(a.addEventListener(f.events.change,c,!1),a.addEventListener(f.events.error,d,!1),a[j.enabled]=a[f.enabled],a[j.element]=a[f.element],a[j.exit]=function(){var b=a[f.exit]();return !b&&Promise?new Promise(e(j.exit)):b},Element.prototype[j.request]=function(){var a=this[f.request].apply(this,arguments);return !a&&Promise?new Promise(e(j.request)):a}),f}(document);

    /* src/Video.svelte generated by Svelte v3.42.4 */

    const { window: window_1 } = globals;
    const file$1 = "src/Video.svelte";

    function create_fragment$1(ctx) {
    	let div1;
    	let div0;
    	let video;
    	let t;
    	let activeelements;
    	let div0_style_value;
    	let div0_resize_listener;
    	let current;
    	let mounted;
    	let dispose;
    	add_render_callback(/*onwindowresize*/ ctx[9]);

    	activeelements = new ActiveElements({
    			props: {
    				currentTime: /*currentTime*/ ctx[3],
    				myPlayer: /*myPlayer*/ ctx[2],
    				playerWidth: /*playerWidth*/ ctx[7],
    				containerRef: /*containerRef*/ ctx[4]
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div1 = element("div");
    			div0 = element("div");
    			video = element("video");
    			t = space();
    			create_component(activeelements.$$.fragment);
    			attr_dev(video, "id", "player");
    			attr_dev(video, "class", "azuremediaplayer amp-default-skin");
    			video.playsInline = true;
    			add_location(video, file$1, 109, 4, 3761);
    			attr_dev(div0, "class", "overlay svelte-1set8vs");
    			attr_dev(div0, "style", div0_style_value = `width: ${/*windowHeight*/ ctx[0] * 640 / 360}px; height: ${/*videoHeight*/ ctx[8]}px`);
    			add_render_callback(() => /*div0_elementresize_handler*/ ctx[10].call(div0));
    			add_location(div0, file$1, 107, 2, 3583);
    			attr_dev(div1, "class", "container svelte-1set8vs");
    			toggle_class(div1, "isFullScreen", /*isFullScreen*/ ctx[6]);
    			toggle_class(div1, "isFullScreenIos", /*isFullScreenIos*/ ctx[5]);
    			toggle_class(div1, "isIos", isIos());
    			add_location(div1, file$1, 106, 0, 3469);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div1, anchor);
    			append_dev(div1, div0);
    			append_dev(div0, video);
    			append_dev(div0, t);
    			mount_component(activeelements, div0, null);
    			div0_resize_listener = add_resize_listener(div0, /*div0_elementresize_handler*/ ctx[10].bind(div0));
    			/*div1_binding*/ ctx[11](div1);
    			current = true;

    			if (!mounted) {
    				dispose = listen_dev(window_1, "resize", /*onwindowresize*/ ctx[9]);
    				mounted = true;
    			}
    		},
    		p: function update(ctx, [dirty]) {
    			const activeelements_changes = {};
    			if (dirty & /*currentTime*/ 8) activeelements_changes.currentTime = /*currentTime*/ ctx[3];
    			if (dirty & /*myPlayer*/ 4) activeelements_changes.myPlayer = /*myPlayer*/ ctx[2];
    			if (dirty & /*playerWidth*/ 128) activeelements_changes.playerWidth = /*playerWidth*/ ctx[7];
    			if (dirty & /*containerRef*/ 16) activeelements_changes.containerRef = /*containerRef*/ ctx[4];
    			activeelements.$set(activeelements_changes);

    			if (!current || dirty & /*windowHeight, videoHeight*/ 257 && div0_style_value !== (div0_style_value = `width: ${/*windowHeight*/ ctx[0] * 640 / 360}px; height: ${/*videoHeight*/ ctx[8]}px`)) {
    				attr_dev(div0, "style", div0_style_value);
    			}

    			if (dirty & /*isFullScreen*/ 64) {
    				toggle_class(div1, "isFullScreen", /*isFullScreen*/ ctx[6]);
    			}

    			if (dirty & /*isFullScreenIos*/ 32) {
    				toggle_class(div1, "isFullScreenIos", /*isFullScreenIos*/ ctx[5]);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(activeelements.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(activeelements.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div1);
    			destroy_component(activeelements);
    			div0_resize_listener();
    			/*div1_binding*/ ctx[11](null);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$1.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    const videoUrl = 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/dadac310-d1c3-45e0-82d9-1817c1b4a0e3/1_Обжорство Комменты.ism/manifest(format=m3u8-aapl,audio-only=false)';

    function instance$1($$self, $$props, $$invalidate) {
    	let videoHeight;
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('Video', slots, []);
    	let myPlayer;
    	let currentTime = 0;
    	let containerRef;
    	let isFullScreenIos = false;
    	let isFullScreen = false;
    	let playerWidth;
    	let windowHeight;
    	let windowWidth;
    	let animationFrameId;

    	const onTimeUpdate = () => {
    		$$invalidate(3, currentTime = myPlayer.currentTime());
    		animationFrameId = requestAnimationFrame(onTimeUpdate);
    	};

    	const forwardEvent = event => {
    		const newEvent = event.touches
    		? new TouchEvent(event.type, event)
    		: new MouseEvent(event.type, event);

    		containerRef.querySelector('.vjs-slider').dispatchEvent(newEvent);
    	};

    	const onCanplaythrough = () => {
    		const element = containerRef.querySelector('.vjs-fullscreen-control');
    		const clone = element.cloneNode(true);
    		element.parentNode.replaceChild(clone, element);

    		['.vjs-current-time', '.vjs-time-divider', '.vjs-duration'].forEach(selector => {
    			

    			[
    				'mousedown',
    				'mouseup',
    				'touchstart',
    				'touchmove',
    				'touchend',
    				'mousemove',
    				'click'
    			].forEach(event => {
    				containerRef.querySelector(selector).addEventListener(event, forwardEvent);
    			});
    		});
    	};

    	const init = () => {
    		if (!window.amp) {
    			return setTimeout(init, 10);
    		}

    		$$invalidate(2, myPlayer = window.amp(
    			'player',
    			{
    				nativeControlsForTouch: false,
    				autoplay: true,
    				muted: true,
    				controls: true,
    				fluid: true,
    				height: windowHeight * 360 / 640,
    				width: windowHeight * 640 / 360,
    				logo: { enabled: false }
    			},
    			function () {
    				window.azure = myPlayer;
    				onTimeUpdate();
    				this.removeEventListener('canplaythrough', onCanplaythrough);
    				this.addEventListener('canplaythrough', onCanplaythrough);
    			}
    		));

    		myPlayer.src([
    			{
    				src: videoUrl,
    				type: 'application/vnd.ms-sstr+xml'
    			}
    		]);

    		window.azure = myPlayer;
    	};

    	init();

    	const onFullScreen = event => {
    		if (event.target.classList.contains('vjs-fullscreen-control') || event.target.parentElement.classList.contains('vjs-fullscreen-control')) {
    			if (isIos()) {
    				$$invalidate(5, isFullScreenIos = !isFullScreenIos);
    				return;
    			}

    			if (document.fullscreenElement || document.webkitCurrentFullScreenElement) {
    				$$invalidate(6, isFullScreen = false);
    				document.exitFullscreen();
    			} else {
    				$$invalidate(6, isFullScreen = true);
    				containerRef.requestFullscreen();
    			}
    		}
    	};

    	const onExitFullScreen = () => {
    		$$invalidate(6, isFullScreen = document.fullscreenElement || document.webkitCurrentFullScreenElement);
    	};

    	onMount(() => {
    		containerRef.addEventListener('click', onFullScreen, true);
    		document.addEventListener('fullscreenchange', onExitFullScreen);
    		document.addEventListener('webkitfullscreenchange', onExitFullScreen);

    		return () => {
    			cancelAnimationFrame(animationFrameId);
    			containerRef.removeEventListener('click', onFullScreen);
    			document.removeEventListener('fullscreenchange', onExitFullScreen);
    			document.removeEventListener('webkitfullscreenchange', onExitFullScreen);
    		};
    	});

    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<Video> was created with unknown prop '${key}'`);
    	});

    	function onwindowresize() {
    		$$invalidate(0, windowHeight = window_1.innerHeight);
    		$$invalidate(1, windowWidth = window_1.innerWidth);
    	}

    	function div0_elementresize_handler() {
    		playerWidth = this.clientWidth;
    		$$invalidate(7, playerWidth);
    	}

    	function div1_binding($$value) {
    		binding_callbacks[$$value ? 'unshift' : 'push'](() => {
    			containerRef = $$value;
    			$$invalidate(4, containerRef);
    		});
    	}

    	$$self.$capture_state = () => ({
    		ActiveElements,
    		isIos,
    		onMount,
    		videoUrl,
    		myPlayer,
    		currentTime,
    		containerRef,
    		isFullScreenIos,
    		isFullScreen,
    		playerWidth,
    		windowHeight,
    		windowWidth,
    		animationFrameId,
    		onTimeUpdate,
    		forwardEvent,
    		onCanplaythrough,
    		init,
    		onFullScreen,
    		onExitFullScreen,
    		videoHeight
    	});

    	$$self.$inject_state = $$props => {
    		if ('myPlayer' in $$props) $$invalidate(2, myPlayer = $$props.myPlayer);
    		if ('currentTime' in $$props) $$invalidate(3, currentTime = $$props.currentTime);
    		if ('containerRef' in $$props) $$invalidate(4, containerRef = $$props.containerRef);
    		if ('isFullScreenIos' in $$props) $$invalidate(5, isFullScreenIos = $$props.isFullScreenIos);
    		if ('isFullScreen' in $$props) $$invalidate(6, isFullScreen = $$props.isFullScreen);
    		if ('playerWidth' in $$props) $$invalidate(7, playerWidth = $$props.playerWidth);
    		if ('windowHeight' in $$props) $$invalidate(0, windowHeight = $$props.windowHeight);
    		if ('windowWidth' in $$props) $$invalidate(1, windowWidth = $$props.windowWidth);
    		if ('animationFrameId' in $$props) animationFrameId = $$props.animationFrameId;
    		if ('videoHeight' in $$props) $$invalidate(8, videoHeight = $$props.videoHeight);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	$$self.$$.update = () => {
    		if ($$self.$$.dirty & /*windowHeight, windowWidth*/ 3) {
    			$$invalidate(8, videoHeight = windowHeight > windowWidth
    			? windowWidth * 360 / 640
    			: windowHeight);
    		}
    	};

    	return [
    		windowHeight,
    		windowWidth,
    		myPlayer,
    		currentTime,
    		containerRef,
    		isFullScreenIos,
    		isFullScreen,
    		playerWidth,
    		videoHeight,
    		onwindowresize,
    		div0_elementresize_handler,
    		div1_binding
    	];
    }

    class Video extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$1, create_fragment$1, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Video",
    			options,
    			id: create_fragment$1.name
    		});
    	}
    }

    /* src/App.svelte generated by Svelte v3.42.4 */
    const file = "src/App.svelte";

    function create_fragment(ctx) {
    	let link0;
    	let script;
    	let script_src_value;
    	let link1;
    	let t;
    	let video;
    	let current;
    	video = new Video({ $$inline: true });

    	const block = {
    		c: function create() {
    			link0 = element("link");
    			script = element("script");
    			link1 = element("link");
    			t = space();
    			create_component(video.$$.fragment);
    			attr_dev(link0, "href", "//amp.azure.net/libs/amp/latest/skins/amp-default/azuremediaplayer.min.css");
    			attr_dev(link0, "rel", "stylesheet");
    			add_location(link0, file, 5, 1, 71);
    			if (!src_url_equal(script.src, script_src_value = "//amp.azure.net/libs/amp/latest/azuremediaplayer.min.js")) attr_dev(script, "src", script_src_value);
    			add_location(script, file, 6, 1, 180);
    			attr_dev(link1, "href", "https://fonts.googleapis.com/css?family=Roboto");
    			attr_dev(link1, "rel", "stylesheet");
    			add_location(link1, file, 7, 1, 254);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			append_dev(document.head, link0);
    			append_dev(document.head, script);
    			append_dev(document.head, link1);
    			insert_dev(target, t, anchor);
    			mount_component(video, target, anchor);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(video.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(video.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			detach_dev(link0);
    			detach_dev(script);
    			detach_dev(link1);
    			if (detaching) detach_dev(t);
    			destroy_component(video, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots('App', slots, []);
    	const writable_props = [];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== '$$' && key !== 'slot') console.warn(`<App> was created with unknown prop '${key}'`);
    	});

    	$$self.$capture_state = () => ({ Video });
    	return [];
    }

    class App extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance, create_fragment, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "App",
    			options,
    			id: create_fragment.name
    		});
    	}
    }

    const app = new App({
    	target: document.getElementById('playerContainer'),
    	props: {},
    });

    return app;

}());
//# sourceMappingURL=bundle.js.map
