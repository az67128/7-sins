export default [
    {
        name: "Александр Дуб",
        avatar: '01.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/7583e301-eea3-440c-9574-5a21475622b7/Sephora 001 010.ism/manifest(format=m3u8-aapl,audio-only=false)',
    },
    {
        name: "Макс Иванов",
        avatar: '02.png',
        friendCount: 1,
    },
    {
        name: "Ира Смолова",
        avatar: '03.png',
        friendCount: 3,
    },
    {
        name: "Игорь Самойлов",
        avatar: '04.png',
        friendCount: 1,
    },
    {
        name: "Jane Moreynis",
        avatar: '05.png',
        friendCount: 1,
    },
    {
        name: "Яна Костылева",
        avatar: '06.png',
        friendCount: 1,
    },

    {
        name: "Роман Ненашев",
        avatar: '07.png',
        friendCount: 1,
    },
    {
        name: "Аня Китаева",
        avatar: '08.png',
        friendCount: 1,
    },
    {
        name: "Айаз Кая",
        avatar: '09.png',
        friendCount: 13,
    },
    {
        name: "Челси Мэннинг",
        avatar: '10.png',
        friendCount: 2,
    },
    {
        name: "Иван Колесов",
        avatar: '11.png',
        friendCount: 1,
    },
    {
        name: "Klaus Jones",
        avatar: '12.png',
        friendCount: 1,
    },

    {
        name: "Джулиан Уайт",
        avatar: '13.png',
        friendCount: 1,
    },
    {
        name: "Рита Симоненко",
        avatar: '14.png',
        friendCount: 0,
    },
    {
        name: "Егор Горин",
        avatar: '15.png',
        friendCount: 1,
    },
    {
        name: "Артур Николаев",
        avatar: '16.png',
        friendCount: 1,
    },
    {
        name: "Армен Абрамян",
        avatar: '17.png',
        friendCount: 1,
    },
    {
        name: "Dude Dudenstein",
        avatar: '18.png',
        friendCount: 1,
    },

    {
        name: "Виктория Ефремова",
        avatar: '19.png',
        friendCount: 1,
    },
    {
        name: "Tema Razumovsky",
        avatar: '20.png',
        friendCount: 1,
    },
    {
        name: "Георгий Шотов",
        avatar: '21.png',
        friendCount: 1,
    },
    {
        name: "Виталий Герасимов",
        avatar: '22.png',
        friendCount: 0,
    },
    {
        name: "Полина Ершова",
        avatar: '23.png',
        friendCount: 1,
    },
    {
        name: "Светлана Хлюпина",
        avatar: '24.png',
        friendCount: 1,
        videoUrl: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/7583e301-eea3-440c-9574-5a21475622b7/Sephora 001 010.ism/manifest(format=m3u8-aapl,audio-only=false)',
    },
]
