

export const isDev = () => {
  return (location.href.includes("localhost") || location.href.includes("192.168"))
}

export const timecodeToSeconds = (timecode) => {
    return timecode.split(':')
        .reverse()
        .reduce((acc, current, index) => Number(acc) + current*(60**index))
}

export const secondsToTimecode = (time) => {
    const hours = Math.floor(time / 60 / 60);
    const minutes = Math.floor(time / 60) - (hours * 60);
    const seconds = Math.floor(time % 60);

    return [
        hours.toString().padStart(1, '0'),
        minutes.toString().padStart(2, '0'),
        seconds.toString().padStart(2, '0')
      ].join(':');
}

export const BASE_URL = (() => {
    if (location.href.includes("gitlab"))
      return "https://az67128.gitlab.io/7-sins";
    if (location.href.includes("localhost") || location.href.includes("192.168"))
      return ".";
    return "https://az67128.gitlab.io/7-sins";
  })();


  export function isIos() {
    return (
      [
        "iPad Simulator",
        "iPhone Simulator",
        "iPod Simulator",
        "iPad",
        "iPhone",
        "iPod",
      ].includes(navigator.platform) ||
      // iPad on iOS 13 detection
      (navigator.userAgent.includes("Mac") && "ontouchend" in document)
    );
  }
