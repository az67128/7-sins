import App from './App.svelte';

const app = new App({
	target: document.getElementById('playerContainer'),
	props: {},
});

export default app;
